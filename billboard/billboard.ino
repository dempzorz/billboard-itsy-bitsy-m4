//#include <WiFiNINA.h>

#include "Src/Billboard.h"

namespace
{
   Billboard billboard;
}

// the setup function runs once when you press reset or power the board
void setup() {
   // initialize digital pin 13 as an output.
   pinMode(13, OUTPUT);

   Serial.begin(9600);

   //while (!Serial) {
   //   delay(10); // wait for serial port to connect. Needed for native USB port only
  // }

   Serial.println("Initialising");
   
   bool result = billboard.Init();
   Serial.print("Init result: ");
   Serial.println(result);
}

// the loop function runs over and over again forever
void loop()
{
  billboard.Update();
  //Serial.println("Looping");
   //billboard.Update();
   delay(1000 * 60 * 5);

   //constexpr unsigned int delayTime = 1000 * 60;// *20;  // 20 mins
   //billboard.Sleep(delayTime);
}
