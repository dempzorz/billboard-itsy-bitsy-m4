#pragma once

#include <cstdint>
#include <cstdlib>
#include <array>

template<size_t ImageWidth, size_t ImageHeight>
class ArrayImage
{
public:
   constexpr static size_t width = ImageWidth;
   constexpr static size_t height = ImageHeight;
   constexpr static size_t bufferSize = ((ImageWidth + 1) / 2 * ImageHeight);

   constexpr ArrayImage(std::array< uint8_t, bufferSize>&& data) :
      data(std::move(data))
   {
   }

   constexpr size_t Width() const { return width; };
   constexpr size_t Height() const { return height; };
   //constexpr const uint8_t* Data() const { return data.data(); };
   constexpr const uint8_t* Data() const { return data.data(); };

private:
   std::array< uint8_t, bufferSize> data;
};


class Glyph
{
public:
   constexpr Glyph(const uint8_t* data, size_t glyphWidth, size_t glyphHeight, size_t displayWidth, size_t displayXOffset, size_t displayYOffset) :
      data(data),
      glyphWidth(glyphWidth),
      glyphHeight(glyphHeight),
      displayWidth(displayWidth),
      displayXOffset(displayXOffset),
      displayYOffset(displayYOffset)
   {
   }

   constexpr Glyph(const uint8_t* data, size_t glyphWidth, size_t glyphHeight) :
      data(data),
      glyphWidth(glyphWidth),
      glyphHeight(glyphHeight),
      displayWidth(glyphWidth)
   {
   }

   template<size_t Width, size_t Height>
   constexpr Glyph(const ArrayImage<Width, Height>& arrayImage) :
      data(arrayImage.Data()),
      glyphWidth(Width),
      glyphHeight(Height),
      displayWidth(glyphWidth)
   {
   }

   constexpr size_t DataLenth() const
   {
      return ((glyphWidth + 1) / 2 * glyphHeight);
   }

   constexpr size_t Width() const { return displayWidth; };
   constexpr size_t Height() const { return glyphHeight; };
   constexpr const uint8_t* Data() const { return data; };

   const uint8_t* data;
   size_t glyphWidth;
   size_t glyphHeight;
   size_t displayWidth = 0;
   size_t displayXOffset = 0;
   size_t displayYOffset = 0;
};

constexpr uint8_t emptyGlyphArray[] = {};
constexpr Glyph emptyGlyph(emptyGlyphArray, 0, 0, 0, 0, 0);

class Buffer
{
public:
   constexpr Buffer(uint8_t* data, size_t width, size_t height) :
      data(data),
      width(width),
      height(height)
   {
   }

   constexpr size_t Width() const { return width; };
   constexpr size_t Height() const { return height; };
   //constexpr const uint8_t* Data() const { return data; };
   constexpr uint8_t* Data() { return data; };

   constexpr size_t DataLenth() const
   {
      return ((width + 1) / 2 * height);
   }

private:
   uint8_t* data;
   const size_t width;
   const size_t height;
};

template<typename T>
struct GlyphLoader
{
   constexpr GlyphLoader(T&& glyphs) 
     : m_glyphs(std::move(glyphs))
   {
   }

   constexpr const Glyph& GetGlyph(uint8_t accessor) const {
      return m_glyphs.GetGlyph(accessor);
   }

   T m_glyphs;
};
