
#include "Billboard.h"

#include "BillboardRenderer.h"
#include "CircularBufferTests.h"

#include "HardwareDefinitions.h"

#include <ArduinoJson.h>
#include <Adafruit_SleepyDog.h>

namespace
{
   constexpr char kSSID[] = "dempzorz";
   constexpr char kPassword[] = "10001110101";
   constexpr uint16_t kPort = 31384;

   constexpr size_t bufferSize = 30000;

   void printJSonObject(JsonObject object, int depth);

   void printJSonVariant(JsonVariant variant, int depth = 0) {
      

      if (variant.is<const char*>()) {
         Serial.println(variant.as<const char*>());
      }
      else if (variant.is<double>()) {
         Serial.println(variant.as<double>());
      }
      else if (variant.is<int>()) {
         Serial.println(variant.as<int>());
      }
      else if (variant.is<bool>()) {
         Serial.println(variant.as<bool>());
      }
      else if (variant.is<JsonArray>()) {
         Serial.println("Array");
         auto jsonArray = variant.as<JsonArray>();
         for (JsonVariant item : jsonArray) {
            printJSonVariant(item, depth);
         }
      }
      else if (variant.is<JsonObject>()) {
         printJSonObject(variant.as<JsonObject>(), depth + 1);
      }
   }

   void printJSonObject(JsonObject object, int depth = 0)
   {
      for (ArduinoJson::JsonPair p : object) {
         
         for (int i = 0; i < depth; ++i)
         {
            Serial.print("\t");
         }

         Serial.print("Key: ");
         Serial.print(p.key().c_str()); // is a JsonString
         Serial.print(", Value: ");
         printJSonVariant(p.value(), depth);
      }
   }

   void EnableLED()
   {
      digitalWrite(13, HIGH);
   }

   void DisableLED()
   {
      digitalWrite(13, LOW);
   }

   void BlinkForever(unsigned long blinkIntervalMs)
   {
      while (true)
      {
         EnableLED();
         delay(blinkIntervalMs);
         DisableLED();
         delay(blinkIntervalMs);
      }
   }
}


Billboard::Billboard() :
   m_epd(Itsy32u::Pins::Reset, Itsy32u::Pins::DataCommand, Itsy32u::Pins::EPDChipSelect, Itsy32u::Pins::Busy),
   m_airlift(Itsy32u::Pins::AirliftChipSelect, Itsy32u::Pins::Reset, Itsy32u::Pins::Busy)
{
   pinMode(13, OUTPUT);
   DisableLED();
}

bool Billboard::Init()
{
   Serial.println("Initialising epd");

   if (!m_epd.Init())
   {
      Serial.println("Failed to init epd");
      BlinkForever(100);
   }
   Serial.println("Initialising airlift");
   if (!m_airlift.Init())
   {
      Serial.println("Failed to init airlift");
      BlinkForever(100);
   }

   if (!m_airlift.ConnectToNetwork(kSSID, kPassword))
   {
      m_airlift.DisconnectFromNetwork();
      Serial.println("Failed to connect to network");
      return false;
   }

   Serial.print("IPAddress: ");
   Serial.println(m_airlift.LocalIpAddress());

   //CircularBufferTests::run();

   return true;
}

bool Billboard::Update()
{
  Serial.println("Update...");

   constexpr float OUR_HOUSE_LAT = 40.032652;
   constexpr float OUR_HOUSE_LON = -105.044305;

   char path[50] = { 0 };
   snprintf(path, 50, "/billboard/%.6f/%.6f", OUR_HOUSE_LAT, OUR_HOUSE_LON);

   Serial.print("Connecting to path ");
   Serial.println(path);

   if (!m_airlift.MakeWebRequest("www.dempz.com", path, 443))
   {
      Serial.println("Failed to connect to server");
      return false;
   }

   Serial.println("Connected to server!!");

   ArduinoJson::JsonObject responseJSon;
   static char buffer[bufferSize] = { 0 }; // TODO: Don't zero

   DynamicJsonDocument doc(bufferSize);

   if (m_airlift.WaitForMessage(doc, buffer, bufferSize))
   {
      // Disconnect here, so we don't waste power while rendering
      //m_airlift.DisconnectFromNetwork();
      BillboardRenderer::Render(m_epd.imageBuffer(), doc, m_history);

      Serial.println("Displaying frame");
      m_epd.DisplayFrame(nullptr);
      Serial.println("Displayed frame");
   }
   else 
   {
      // Just let it try again next time.
      Serial.println("Failed to read response");
   }

   return true;
}

void Billboard::Sleep(int millisecondsToSleep)
{
   // TODO: Need to get sleep working properly to massivly cut down on it's power use.
   return;
   Serial.print("millisecondsToSleep: ");
   Serial.println(millisecondsToSleep);

   //m_airlift.DisconnectFromNetwork();
   Serial.println("Sleeping EPD");
   m_epd.Sleep();

   Serial.println("Slept EPD");
   while (millisecondsToSleep >= 0)
   {
      Serial.println("Goodnight");
      millisecondsToSleep -= Watchdog.sleep(millisecondsToSleep);

      Serial.print("millisecondsToSleep: ");
      Serial.println(millisecondsToSleep);
   }

   //Serial.println("Sleeping EPD");
   //m_epd.Sleep();

   Serial.println("Resetting EPD");
   m_epd.Reset(); // Do I need to reinit?

   delay(500);

   if (!m_epd.Init())
   {
      Serial.println("Failed to init airlift");
      BlinkForever(100);
   }

   EnableLED();

#if defined(USBCON) && !defined(USE_TINYUSB)
   USBDevice.attach();
   //Serial.begin(9600);
#endif
   Serial.println("Goodmorning");
}
