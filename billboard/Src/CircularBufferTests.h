
#pragma once

#include "CircularBuffer.h"

#include <string>

namespace CircularBufferTests
{
   void run()
   {
      auto runTest = [](bool test, const std::string& testName)
      {
         Serial.print("TEST ");
         if (!test)
         {
            Serial.print("FAILED - ");
         }
         else
         {
            Serial.print("PASSED - ");
         }

         Serial.println(testName.c_str());
      };

      CircularBuffer<3, int> testBuffer;

      runTest(testBuffer.Count() == 0, "Expect empty");

      testBuffer.Push(1);
      runTest(testBuffer.Count() == 1, "Expect 1");

      testBuffer.Push(2);
      runTest(testBuffer.Count() == 2, "Expect 2");

      testBuffer.Push(3);
      runTest(testBuffer.Count() == 3, "Expect 3");

      size_t current = 1;

      testBuffer.Iterate(5, [&](const int& value)
      {
           runTest(current == value, "Expect value: " + std::to_string(value));
           current++;
      });

      testBuffer.Push(4);
      runTest(testBuffer.Count() == 3, "Expect 3");

      current = 2;

      testBuffer.Iterate(5, [&](const int& value)
      {
         runTest(current == value, "Expect value: " + std::to_string(value));
         current++;
      });

      // Check that we only get the ones we want
      current = 3;

      testBuffer.Iterate(2, [&](const int& value)
      {
         runTest(current == value, "Expect value: " + std::to_string(value));
         current++;
      });

      testBuffer.PopBack();
      runTest(testBuffer.Count() == 2, "Expect 2");

      testBuffer.PopBack();
      runTest(testBuffer.Count() == 1, "Expect 1");

      testBuffer.Iterate(5, [&](const int& value)
      {
         runTest(value == 4, "Expect value: 4 - got " + std::to_string(value));
      });
   }
}
