#pragma once

#include <ArduinoJson.h>

namespace JSonUtils
{
   void Print(const JsonObject& object, int depth = 0);
   void Print(const JsonVariant& variant, int depth = 0);
}