
#include "JSonUtils.h"

namespace JSonUtils
{
   void Print(const JsonObject& object, int depth)
   {
      for (ArduinoJson::JsonPair p : object) {

         for (int i = 0; i < depth; ++i)
         {
            Serial.print("\t");
         }

         Serial.print("Key: ");
         Serial.print(p.key().c_str()); // is a JsonString
         Serial.print(", Value: ");
         Print(p.value(), depth);
      }
   }

   void Print(const JsonVariant& variant, int depth)
   {
      if (variant.is<const char*>()) {
         Serial.println(variant.as<const char*>());
      }
      else if (variant.is<double>()) {
         Serial.println(variant.as<double>());
      }
      else if (variant.is<int>()) {
         Serial.println(variant.as<int>());
      }
      else if (variant.is<bool>()) {
         Serial.println(variant.as<bool>());
      }
      else if (variant.is<JsonArray>()) {
         Serial.println("Array");
         auto jsonArray = variant.as<JsonArray>();
         for (JsonVariant item : jsonArray) {
            Print(item, depth);
         }
      }
      else if (variant.is<JsonObject>()) {
         Print(variant.as<JsonObject>(), depth + 1);
      }
   }
}