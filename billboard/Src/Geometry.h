#pragma once

#include <cstddef>

struct Point
{
   constexpr Point(size_t x, size_t y) :
      x(x),
      y(y)
   {
   }

   size_t x;
   size_t y;
};

struct Region
{
   constexpr Region(Point topLeft, Point bottomRight) :
      topLeft(topLeft),
      bottomRight(bottomRight)
   {
   }

   constexpr Region(size_t topLeftX, size_t topLeftY, size_t bottomRightX, size_t bottomRightY) :
      topLeft(Point(topLeftX, topLeftY)),
      bottomRight(Point(bottomRightX, bottomRightY))
   {
   }

   constexpr size_t Height() const
   {
      return bottomRight.y - topLeft.y + 1;
   }

   constexpr size_t Width() const
   {
      return bottomRight.x - topLeft.x + 1;
   }

   Point topLeft;
   Point bottomRight;
};