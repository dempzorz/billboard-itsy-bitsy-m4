#pragma once

#include "Definitions.h"

#include <ArduinoJson.h>
#include <WiFiNINA.h>

class AirLiftBitsty
{
public:
   AirLiftBitsty(uint8_t chipSelectPin, uint8_t resetPin, uint8_t busyPin);
   ~AirLiftBitsty();

   bool Init();
   bool IsReady() const;
   bool ConnectToNetwork(const char* ssid, const char* password);
   void DisconnectFromNetwork();
   //bool ConnectToTCP(IPAddress server, uint16_t port);
   //bool ConnectToTCP(const char* server, uint16_t port);
   bool MakeWebRequest(const char* server, const char* path, uint16_t port);

   bool SendData(const char* data, size_t size);

   bool IsConnectedToNetwork();
   bool IsConnectedToServer();

   bool WaitForMessage(ArduinoJson::DynamicJsonDocument& result, char* buffer, size_t bufferSize);

   IPAddress LocalIpAddress() const;

   // NOTE: call WiFi.lowPowerMode()

private:
   uint8_t m_resetPin;
   uint8_t m_chipSelectPin;
   uint8_t m_busyPin;

   WiFiSSLClient m_client;
};