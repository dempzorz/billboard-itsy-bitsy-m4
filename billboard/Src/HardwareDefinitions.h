
#pragma once

#include <stdlib.h>

using uint8_t = unsigned char;

namespace Itsy32u
{
   namespace Pins
   {
      constexpr uint8_t Busy = 0x07;
      constexpr uint8_t Reset = 0x05;
      constexpr uint8_t DataCommand = 0x09;
      constexpr uint8_t EPDChipSelect = 0x0A;
      constexpr uint8_t AirliftChipSelect = 0x0D;
   }
}//245760

namespace EPD7IN5B
{
   namespace Dimentions
   {
      constexpr size_t width = 640;
      constexpr size_t height = 384;
      //constexpr int width = 800;
      //constexpr int height = 480;
   }

   namespace Commands
   {
      namespace PanelSetting
      {
         constexpr uint8_t Id = 0x00;

         enum class ResolutionSetting : uint8_t
         {
            _640x480 = 0,
            _600x450 = 0x40,
            _640x448 = 0x80,
            _600x448 = 0xC0
         };

         enum class LUTSelection : uint8_t
         {
            ExternalFlash = 0,
            Register = 0x20
         };

         enum class GateScanDirection : uint8_t
         {
            Down = 0,
            Up = 0x08
         };

         enum class SourceShiftDirection : uint8_t
         {
            Left = 0,
            Right = 0x04
         };

         enum class BoosterSwitch : uint8_t
         {
            ConverterOff = 0,
            ConverterOn = 0x02
         };

         constexpr uint8_t Reset = 0x01;
      }

      namespace PowerSetting
      {
         constexpr uint8_t Id = 0x01;


      }

      //constexpr uint8_t PanelSetting            = 0x00;
      //constexpr uint8_t PowerSetting            = 0x01;
      constexpr uint8_t PowerOff                = 0x02;
      constexpr uint8_t PowerOffSequenceSetting = 0x03;
      constexpr uint8_t PowerOn                 = 0x04;
      constexpr uint8_t PowerOnMeasure          = 0x05;
      constexpr uint8_t BoosterSoftStart        = 0x06;
      constexpr uint8_t DeepSleep               = 0x07;


      constexpr uint8_t DataStartTransmission1  = 0x10;
      constexpr uint8_t DataStop                = 0x11;
      constexpr uint8_t DisplayRefresh          = 0x12;
      constexpr uint8_t ImageProcess            = 0x13;

      namespace LUT
      {
         constexpr uint8_t ForVCom              = 0x20;
         constexpr uint8_t Blue                 = 0x21;
         constexpr uint8_t White                = 0x22;
         constexpr uint8_t Grey1                = 0x23;
         constexpr uint8_t Grey2                = 0x24;
         constexpr uint8_t Red0                 = 0x25;
         constexpr uint8_t Red1                 = 0x26;
         constexpr uint8_t Red2                 = 0x27;
         constexpr uint8_t Red3                 = 0x28;
         constexpr uint8_t Xon                  = 0x29;
      }

      constexpr uint8_t PLLControl              = 0x30;

      namespace Temperature
      {
         constexpr uint8_t SensorCommand        = 0x40;
         constexpr uint8_t Calibration          = 0x41;
         constexpr uint8_t SensorWrite          = 0x42;
         constexpr uint8_t SensorRead           = 0x43;
      }

      constexpr uint8_t VComAndDataIntervalSettings = 0x50;
      constexpr uint8_t LowPowerDetection       = 0x51;

      constexpr uint8_t TConSetting             = 0x60;
      constexpr uint8_t TConResolution          = 0x61;

      constexpr uint8_t SPIFlashControl         = 0x65;

      constexpr uint8_t Revision                = 0x70;
      constexpr uint8_t GetStatus               = 0x71;

      constexpr uint8_t AutoMeasureVCom         = 0x80;
      constexpr uint8_t ReadVComValue           = 0x81;
      constexpr uint8_t VComDCSettings          = 0x82;

      constexpr uint8_t FlashMode               = 0xe5;
   }

   /*namespace Pins
   {
      constexpr uint8_t Busy        = 0x07;
      constexpr uint8_t Reset       = 0x08;
      constexpr uint8_t DataCommand = 0x09;
      constexpr uint8_t ChipSelect  = 0x0A;
   }*/
}