
#include "BillboardRenderer.h"

#include "Images/sf_pixelate_regular_21.h"
#include "Images/trebuchet_ms_regular_61.h"
#include "Images/weather_icons.h"

#include "ImageUtils.h"
#include "WeatherIcons.h"

#include <ctime>
#include <cmath>
#include <string>
#include <vector>

namespace
{
   namespace Attributes
   {
      constexpr size_t BorderWidth = 3;
      constexpr size_t StatusBarHeight = 30;
      constexpr size_t MainSectionHeight = 235;
      constexpr ImageUtils::Color BorderColour = ImageUtils::Color::Black;
      constexpr size_t DailySectionCount = 6;
      constexpr size_t DailyWidth = (EPD7IN5B::Dimentions::width - ((DailySectionCount - 1) * BorderWidth)) / DailySectionCount;

   }

   constexpr size_t HourlyTempSize = 2;

   namespace Positions
   {
      /*namespace Sections
      {
         constexpr Region StatusBar(Point(0, 0), Point(EPD7IN5B::Dimentions::width - 1, Attributes::StatusBarHeight));

         constexpr Region Dailys(Point(0, MainRegion.bottomRight.y + Attributes::BorderWidth), Point(EPD7IN5B::Dimentions::width - 1, EPD7IN5B::Dimentions::height - 1 - MainRegion.bottomRight.y - Attributes::BorderWidth));
      }*/

      namespace StatusBar
      {
         constexpr Region Area(Point(0, 0), Point(EPD7IN5B::Dimentions::width - 1, Attributes::StatusBarHeight));

         // Relative to region
         constexpr Point Date(5, 3);
         constexpr Point WifiIndicator(540, 7);
         constexpr Point BatteryIndicator(580, 7);
      }

      namespace MainRegion
      {
         constexpr Region Area(Point(0, StatusBar::Area.bottomRight.y + Attributes::BorderWidth), Point(EPD7IN5B::Dimentions::width - 1, Attributes::MainSectionHeight));

         // Relative to region
         constexpr Region WeatherIcon(Point(5, 3), Point(5, 3)); // TODO
         constexpr Point CurrentTemperature(600, 3); // TODO
         constexpr Point FeelsLike(600, 3); // TODO
         constexpr Region HourlyGraph(Point(5, 3), Point(5, 3)); // TODO
      }

      namespace Dailys
      {
         constexpr Region FullArea(Point(0, MainRegion::Area.bottomRight.y + Attributes::BorderWidth), Point(EPD7IN5B::Dimentions::width - 1, EPD7IN5B::Dimentions::height - 1 - MainRegion::Area.bottomRight.y - Attributes::BorderWidth));
         constexpr Region WeatherIcon(Point(5, 3), Point(5, 3)); // TODO
         constexpr Point DayAndTemp(600, 3); // TODO
      }
   }

   // Adapted from https://stackoverflow.com/a/15528789/2480711
   std::vector<Point> getCurvePoints(std::vector<Point>& points, float tension, int numOfSegments)
   {
      std::vector<Point> results;

      results.reserve(points.size() * numOfSegments);

      if (!points.empty() && numOfSegments > 0)
      {
         Serial.print("Point count: ");
         Serial.println(points.size());

         points.push_back(points.back());

         // 1. loop goes through point array
         // 2. loop goes through each segment between the 2 pts + 1e point before and after
         for (int i = 0; i < (points.size() - 2); ++i)
         {
            for (int segment = 0; segment <= numOfSegments; segment++)
            {
               Serial.print("segment: ");
               Serial.println(segment);
               Serial.print("i: ");
               Serial.println(i);
               const Point& previous = i == 0 ? points.front() : points[i - 1];
               Serial.print("previous x: ");
               Serial.println(previous.x);
               Serial.print("previous y: ");
               Serial.println(previous.y);
               // calc tension vectors
               float t1x = ((float)points[i + 1].x - (float)previous.x) * tension;
               float t2x = ((float)points[i + 2].x - (float)points[i].x) * tension;

               float t1y = ((float)points[i + 1].y - (float)previous.y) * tension;
               float t2y = ((float)points[i + 2].y - (float)points[i].y) * tension;

               // calc step
               float step = (float)segment / (float)numOfSegments;

               Serial.print("step: ");
               Serial.println(step);

               // calc cardinals
               float c1 = 2.0f * std::pow(step, 3) - 3.0f * std::pow(step, 2) + 1;
               float c2 = -(2.0f * std::pow(step, 3)) + 3.0f * std::pow(step, 2);
               float c3 = std::pow(step, 3) - 2.0f * std::pow(step, 2) + step;
               float c4 = std::pow(step, 3) - std::pow(step, 2);

               Serial.println("final");

               // calc x and y cords with common control vectors
               size_t x = c1 * points[i].x + c2 * points[i + 1].x + c3 * t1x + c4 * t2x;
               size_t y = c1 * points[i].y + c2 * points[i + 1].y + c3 * t1y + c4 * t2y;
               Serial.println("adding");
               results.emplace_back(x, y);
               Serial.println("done");
            }
         }
      }

      Serial.print("Finished curve with points: ");
      Serial.println(results.size());

      return results;
   }

   void renderGraphPoints(Buffer& buffer, std::vector<Point>& points, ImageUtils::Color color)
   {
      Serial.println("Getting curve points");
      std::vector<Point> curve = getCurvePoints(points, 1.0f, 16);
      Serial.println("Got curve points");
      if (!curve.empty())
      {
         Serial.print("Point count: ");
         Serial.println(curve.size());
         for (size_t i = 0; i < curve.size() - 1; ++i)
         {
            Serial.println(i);

            float xDif = (float)curve[i + 1].x - (float)curve[i].x;
            float yDif = (float)curve[i + 1].y - (float)curve[i].y;

            if (xDif == 0 && yDif == 0)
            {
               continue;
            }

            float xStep;
            float yStep;
            Serial.println("a");
            // Normalise
            if (abs(xDif) > abs(yDif))
            {
               if (xDif != 0)
               {
                  yStep = yDif / xDif;
                  xStep = 1;
               }
               else
               {
                  yStep = yDif < 0 ? -1 : 1;
                  xStep = 0;
               }
            }
            else
            {
               if (yDif != 0)
               {
                  yStep = yDif < 0 ? -1 : 1;
                  xStep = fabs(xDif / yDif);
               }
               else
               {
                  yStep = 0;
                  xStep = 1;
               }
            }

            float xPos = curve[i].x;
            float yPos = curve[i].y;

            auto floatEqual = [](float f1, float f2)
            {
               return std::fabs(f1 - f2) < 0.01;
            };

            bool goingUp = yDif < 0;

            bool hitY = false;

            do
            {
               Serial.println("b");
               ImageUtils::FillRegion(buffer, Region(xPos, yPos, xPos + HourlyTempSize, yPos + HourlyTempSize), color);

               xPos += xStep;
               yPos += yStep;

               hitY = goingUp && (int)yPos <= (int)curve[i + 1].y;

               hitY |= !goingUp && (int)yPos >= (int)curve[i + 1].y;

            } while ((int)xPos < (int)curve[i + 1].x || !hitY);
         }
      }

      Serial.println("Done with curve");
   }

   std::string getDisplayDate(const JsonDocument& jsonDoc, tm& timeInfo)
   {
      constexpr const char* days[] = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
      constexpr const char* months[] = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };

      std::string displayDate = std::string(days[timeInfo.tm_wday]) + " " + std::to_string(timeInfo.tm_mday);

      int day = timeInfo.tm_mday;

      if (day < 11 || day > 13) {
         day = day % 10;
      
         if (day == 1) {
            displayDate += "st";
         } else if (day == 2) {
            displayDate += "nd";
         } else if (day == 3) {
            displayDate += "rd"	;
         } else {
            displayDate += "th";
         }
      } else {
         displayDate += "th";
      }

      displayDate += " " + std::string(months[timeInfo.tm_mon]);
      return displayDate;
   }

   void RenderStatusBar(Buffer& buffer, const JsonDocument& jsonDoc, tm& timeInfo)
   {
      constexpr auto loader = GlyphLoader(SmallFont::SmallFontLoader());
      constexpr size_t imageWidth = EPD7IN5B::Dimentions::width;

      std::string displayDate = getDisplayDate(jsonDoc, timeInfo);
      ImageUtils::WriteString(displayDate.c_str(), buffer, loader, 5, 8);

      ImageUtils::WriteGlyph(buffer, Icons::wifiImage, Positions::StatusBar::WifiIndicator.x, Positions::StatusBar::WifiIndicator.y);

      // Draw battery
      constexpr size_t BatteryHeight = 18;
      constexpr size_t BatteryWidth = 35; // No terminal
      constexpr size_t PositiveTerminalWidth = 3;
      constexpr size_t PositiveTerminalHeight = BatteryHeight / 3;
      constexpr Point BatteryTopLeft(Positions::StatusBar::BatteryIndicator.x + PositiveTerminalWidth, Positions::StatusBar::BatteryIndicator.y);
      constexpr Point BatteryBottomRight(Positions::StatusBar::BatteryIndicator.x + PositiveTerminalWidth + BatteryWidth, Positions::StatusBar::BatteryIndicator.y + BatteryHeight);
      constexpr Region BatteryLevel(BatteryTopLeft.x + Attributes::BorderWidth + 1, BatteryTopLeft.y + Attributes::BorderWidth + 1,
         BatteryBottomRight.x - Attributes::BorderWidth - 1, BatteryBottomRight.y - Attributes::BorderWidth - 1);
      constexpr Region BatteryTerminal(Positions::StatusBar::BatteryIndicator.x, Positions::StatusBar::BatteryIndicator.y + (BatteryHeight / 2 - PositiveTerminalHeight / 2),
         Positions::StatusBar::BatteryIndicator.x + PositiveTerminalWidth, Positions::StatusBar::BatteryIndicator.y + (BatteryHeight / 2 + PositiveTerminalHeight / 2));

      // Battery Terminal
      ImageUtils::FillRegion(buffer, BatteryTerminal, Attributes::BorderColour);

      // Battery Main outline
      ImageUtils::FillRegion(buffer, Region(BatteryTopLeft, Point(BatteryBottomRight.x, BatteryTopLeft.y + Attributes::BorderWidth)), Attributes::BorderColour);
      ImageUtils::FillRegion(buffer, Region(BatteryTopLeft, Point(BatteryTopLeft.x + Attributes::BorderWidth, BatteryBottomRight.y)), Attributes::BorderColour);
      ImageUtils::FillRegion(buffer, Region(Point(BatteryBottomRight.x - Attributes::BorderWidth, BatteryTopLeft.y), BatteryBottomRight), Attributes::BorderColour);
      ImageUtils::FillRegion(buffer, Region(Point(BatteryTopLeft.x, BatteryBottomRight.y - Attributes::BorderWidth), BatteryBottomRight), Attributes::BorderColour);

      // Battery Level
      constexpr float batteryPercentFull = .8;
      size_t batteryLevelOffset = BatteryLevel.Width() * (1 - batteryPercentFull);
      ImageUtils::FillRegion(buffer, Region(Point(BatteryLevel.topLeft.x + batteryLevelOffset, BatteryLevel.topLeft.y), BatteryLevel.bottomRight), Attributes::BorderColour);

      // Bottom border
      size_t statusBorderTop = Positions::StatusBar::Area.bottomRight.y + 1;
      ImageUtils::FillRegion(buffer, Region(0, statusBorderTop, EPD7IN5B::Dimentions::width, statusBorderTop + Attributes::BorderWidth), Attributes::BorderColour);
   }

   void RenderMainSection(Buffer& buffer, const JsonDocument& jsonDoc, tm& timeInfo, BillboardRenderer::HistoryBuffer& history)
   {
      int weatherType = jsonDoc["current"]["weather"][0]["id"].as<int>();
      double currentTime = jsonDoc["hourly"][0]["dt"].as<double>();
      auto timeOfDay = currentTime < jsonDoc["current"]["sunrise"].as<double>() || currentTime > jsonDoc["current"]["sunset"].as<double>() ? Weather::TimeOfDay::Night : Weather::TimeOfDay::Day;
      ImageUtils::WriteGlyph(buffer, Weather::GetIconFromOpenWeatherId(weatherType, timeOfDay, Weather::Size::Large), 10, 50);
       
      float currentTemp = jsonDoc["current"]["temp"].as<float>();
      constexpr auto largeFontLoader = GlyphLoader(LargeFont::LargeFontLoader());
      char tempString[20] = { 0 };
      snprintf(tempString, 5, "%.1f°", currentTemp);
      ImageUtils::WriteString(tempString, buffer, largeFontLoader, 200, 70);

      
      constexpr auto smallFontLoader = GlyphLoader(SmallFont::SmallFontLoader());
      float feelsLikeTemp = jsonDoc["current"]["feels_like"].as<float>();
      snprintf(tempString, 20, "Feels like: %.1f°", feelsLikeTemp);
      ImageUtils::WriteString(tempString, buffer, smallFontLoader, 200, 150);

      size_t rainSnowLeft = 200;
      constexpr size_t rainSnowTextTop = 206;
      constexpr size_t textAfterIconPadding = 5;
      constexpr size_t rainSnowPadding = 15;
      Serial.println("Get Rain");
      float rainAmount = jsonDoc["daily"][0]["rain"].as<float>();

      Serial.println("Rain and snow");
      if (rainAmount >= 0.1)
      {
         ImageUtils::WriteGlyph(buffer, Icons::largeRainDropImage, rainSnowLeft, 200);
         snprintf(tempString, 5, "%.1f", rainAmount);
         std::string rainString = std::string(tempString) + "mm";
         size_t rainTextLeft = rainSnowLeft + Icons::largeRainDropImage.Width() + textAfterIconPadding;
         ImageUtils::WriteString(rainString.c_str(), buffer, smallFontLoader, rainTextLeft, rainSnowTextTop);
         rainSnowLeft = rainTextLeft + ImageUtils::StringWidth(rainString.c_str(), smallFontLoader) + rainSnowPadding;
      }

      float snowAmount = jsonDoc["daily"][0]["snow"].as<float>();

      if (snowAmount >= 0.1)
      {
         ImageUtils::WriteGlyph(buffer, Icons::largeSnowImage, rainSnowLeft, 200);
         snprintf(tempString, 5, "%.1f", snowAmount);
         std::string snowString = std::string(tempString) + "mm";
         size_t snowTextLeft = rainSnowLeft + Icons::largeSnowImage.Width() + textAfterIconPadding;
         ImageUtils::WriteString(snowString.c_str(), buffer, smallFontLoader, snowTextLeft, rainSnowTextTop);
      }
      Serial.println("Upcoming frame");
      // Upcoming
      constexpr size_t UpcomingBorderWidth = 3;
      constexpr Region UpcomgingRegion(439, 70, 639, 190);
      constexpr size_t NotchSize = 5;
      constexpr Region TopNotch(UpcomgingRegion.topLeft.x - NotchSize, UpcomgingRegion.topLeft.y + NotchSize, UpcomgingRegion.topLeft.x, UpcomgingRegion.topLeft.y + NotchSize * 2);
      constexpr Region BottomNotch(UpcomgingRegion.topLeft.x - NotchSize, UpcomgingRegion.bottomRight.y - NotchSize * 2, UpcomgingRegion.topLeft.x, UpcomgingRegion.bottomRight.y - NotchSize);
      constexpr Region MiddleNotch(UpcomgingRegion.bottomRight.x - UpcomgingRegion.Width() / 2 - NotchSize / 2, UpcomgingRegion.bottomRight.y, UpcomgingRegion.bottomRight.x - UpcomgingRegion.Width() / 2 + NotchSize / 2, UpcomgingRegion.bottomRight.y + NotchSize);
      constexpr size_t TopNotchMiddleY = TopNotch.topLeft.y + TopNotch.Height() / 2;
      constexpr size_t BottomNotchMiddleY = BottomNotch.topLeft.y + BottomNotch.Height() / 2;

      ImageUtils::FillRegion(buffer, Region(UpcomgingRegion.topLeft, Point(UpcomgingRegion.topLeft.x + UpcomingBorderWidth, UpcomgingRegion.bottomRight.y)), Attributes::BorderColour);
      ImageUtils::FillRegion(buffer, Region(Point(UpcomgingRegion.topLeft.x, UpcomgingRegion.bottomRight.y - UpcomingBorderWidth), UpcomgingRegion.bottomRight), Attributes::BorderColour);
      ImageUtils::FillRegion(buffer, TopNotch, Attributes::BorderColour);
      ImageUtils::FillRegion(buffer, BottomNotch, Attributes::BorderColour);
      ImageUtils::FillRegion(buffer, MiddleNotch, Attributes::BorderColour);
      
      size_t upcomingCount = 24 - timeInfo.tm_hour;
      Serial.println("Find current min max");
      float minf = currentTemp;
      float maxf = currentTemp;
      for (size_t i = 0; i < upcomingCount; ++i)
      {
         float value = jsonDoc["hourly"][i]["temp"].as<float>();
         minf = std::min(minf, value);
         maxf = std::max(maxf, value);
      }

      size_t pastHourCount = 24 - upcomingCount;
      Serial.println("Find past min max");
      history.Iterate(pastHourCount, [&](const HistoryItem& value) {
         minf = std::min(minf, value.temperature);
         maxf = std::max(maxf, value.temperature);
      });

      int min = (int)(minf - 0.9f);
      int max = (int)(maxf + 0.9f);

      std::string minString = std::to_string(min);
      std::string maxString = std::to_string(max);

      const char* noonString = "12PM";
      ImageUtils::WriteString(noonString, buffer, smallFontLoader, MiddleNotch.topLeft.x + MiddleNotch.Width() / 2 - ImageUtils::StringWidth(noonString, smallFontLoader) / 2, MiddleNotch.bottomRight.y + 5);
      ImageUtils::WriteString(minString.c_str(), buffer, smallFontLoader, BottomNotch.topLeft.x - ImageUtils::StringWidth(minString.c_str(), smallFontLoader) - 5, BottomNotchMiddleY - ImageUtils::StringHeight(minString.c_str(), smallFontLoader) / 2);
      ImageUtils::WriteString(maxString.c_str(), buffer, smallFontLoader, TopNotch.topLeft.x - ImageUtils::StringWidth(maxString.c_str(), smallFontLoader) - 5, TopNotchMiddleY - ImageUtils::StringHeight(maxString.c_str(), smallFontLoader) / 2);

      std::vector<Point> upcomingPoints;

      auto createPoint = [&](int hour, float temp) {
         constexpr size_t UpdateStep = (UpcomgingRegion.Width() - UpcomingBorderWidth) / 24;
         constexpr size_t WaveHeight = BottomNotchMiddleY - TopNotchMiddleY;

         size_t xPos = UpcomgingRegion.topLeft.x + UpcomingBorderWidth + hour * UpdateStep - HourlyTempSize / 2;

         float valuePercentage = (temp - (float)min) / ((float)max - (float)min);
         size_t yPos = (1 - valuePercentage) * (float)WaveHeight + TopNotchMiddleY;


         Serial.print("creating point for hour: ");
         Serial.print(hour);
         Serial.print(", temp: ");
         Serial.println(temp);
         Serial.print("Point x: ");
         Serial.print(xPos);
         Serial.print(", y: ");
         Serial.println(yPos);

         return Point(xPos, yPos);
      };

      Serial.println("Create upcoming points");

      for (size_t i = 0; i < upcomingCount; ++i)
      {
         time_t unixTime = (time_t)jsonDoc["hourly"][i]["dt"].as<double>();
         unixTime += jsonDoc["timezone_offset"].as<int>();
         tm* hourlyTimeinfo = gmtime(&unixTime);

         float hourlyTemp = jsonDoc["hourly"][i]["temp"].as<float>();
         upcomingPoints.emplace_back(createPoint(hourlyTimeinfo->tm_hour, hourlyTemp));
      }

      Serial.println("Create previous points");

      std::vector<Point> historyPoints;
      history.Iterate(pastHourCount, [&](const HistoryItem& item) {
         Serial.print("history hour: ");
         Serial.print(item.hour);
         Serial.print(", temp: ");
         Serial.println(item.temperature);

         historyPoints.emplace_back(createPoint(item.hour, item.temperature));

         Serial.print("history point x: ");
         Serial.print(historyPoints.back().x);
         Serial.print(", y: ");
         Serial.println(historyPoints.back().y);
      });

      //if (!upcomingPoints.empty()) This is never empty
      {
         time_t unixTime = (time_t)jsonDoc["hourly"][0]["dt"].as<double>();
         unixTime += jsonDoc["timezone_offset"].as<int>();
         tm* hourlyTimeinfo = gmtime(&unixTime);

         Serial.print("history front hour: ");
         Serial.println(history.PeekFront().hour);
         Serial.print("hourlyTimeinfo->tm_hour: ");
         Serial.println(hourlyTimeinfo->tm_hour);

         if (!history.IsEmpty() && history.PeekFront().hour == hourlyTimeinfo->tm_hour)
         {
            Serial.println("Popping front");
            history.PopFront();
         }

         historyPoints.emplace_back(upcomingPoints.front());
         history.Push(HistoryItem(hourlyTimeinfo->tm_hour, jsonDoc["hourly"][0]["temp"].as<float>()));
      }

      Serial.println("rendering history points");

      for (size_t i = 0; i < historyPoints.size(); ++i)
      {
         Serial.print("history point x: ");
         Serial.print(historyPoints[i].x);
         Serial.print(", y: ");
         Serial.println(historyPoints[i].y);
      }

      renderGraphPoints(buffer, historyPoints, ImageUtils::Color::Yellow);

      Serial.println("rendering upcomingPoints");
      renderGraphPoints(buffer, upcomingPoints, Attributes::BorderColour);
      
      // Bottom border
      size_t mainSectionBorderTop = Positions::MainRegion::Area.bottomRight.y + 1;
      ImageUtils::FillRegion(buffer, Region(0, mainSectionBorderTop, EPD7IN5B::Dimentions::width, mainSectionBorderTop + Attributes::BorderWidth), Attributes::BorderColour);
   }

   void RenderDailys(Buffer& buffer, const JsonDocument& jsonDoc, tm& timeInfo)
   {
      constexpr const char* days[] = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };

      constexpr auto smallFontLoader = GlyphLoader(SmallFont::SmallFontLoader());

      for (size_t i = 1; i <= Attributes::DailySectionCount; ++i)
      {
         size_t column = (Attributes::DailyWidth * i) + (Attributes::BorderWidth * (i - 1));

         Serial.print("Filling Daily: ");
         Serial.println(i);

         if (column + Attributes::BorderWidth < EPD7IN5B::Dimentions::width)
         {
            ImageUtils::FillRegion(buffer, Region(column, Positions::Dailys::FullArea.topLeft.y, column + Attributes::BorderWidth, EPD7IN5B::Dimentions::height - 1), Attributes::BorderColour);
         }

         const char* day = days[(timeInfo.tm_wday + i) % 7];
         size_t dayXPos = column - (Attributes::DailyWidth / 2) - (ImageUtils::StringWidth(day, smallFontLoader) / 2);
         ImageUtils::WriteString(day, buffer, smallFontLoader, dayXPos, Positions::Dailys::FullArea.topLeft.y + 10);

         int weatherType = jsonDoc["daily"][i]["weather"][0]["id"].as<int>();
         const auto& icon = Weather::GetIconFromOpenWeatherId(weatherType, Weather::TimeOfDay::Day, Weather::Size::Small);
         size_t tempXPos = column - (Attributes::DailyWidth / 2) - (icon.Width() / 2);
         size_t tempYPos = Positions::Dailys::FullArea.topLeft.y + ((EPD7IN5B::Dimentions::height - Positions::Dailys::FullArea.topLeft.y) / 2) - icon.Height() / 2;
         ImageUtils::WriteGlyph(buffer, icon, tempXPos, tempYPos);

         std::string temp = std::to_string((int)(jsonDoc["daily"][i]["temp"]["max"].as<float>() + 0.5f)) + "°";
         tempXPos = column - (Attributes::DailyWidth / 2) - (ImageUtils::StringWidth(temp.c_str(), smallFontLoader) / 2);

         Serial.println(temp.c_str());
         ImageUtils::WriteString(temp.c_str(), buffer, smallFontLoader, tempXPos, EPD7IN5B::Dimentions::height - 30);

         for (size_t i = 0; i < temp.size(); ++i) {
            Serial.println((int)temp[i]);
         }
      }
   }
}

bool BillboardRenderer::Render(Buffer& buffer, const JsonDocument& jsonDoc, HistoryBuffer& history)
{
   Serial.print("Clearing board: ");
   Serial.println(buffer.DataLenth());
   // Reset all background to white.
   for (size_t i = 0; i < buffer.DataLenth(); ++i)
   {
      buffer.Data()[i] = (ImageUtils::White << 4) | ImageUtils::White;
   }

   time_t unixTime = (time_t)jsonDoc["current"]["dt"].as<double>();
   unixTime += jsonDoc["timezone_offset"].as<int>();

   tm* timeinfo = gmtime(&unixTime);

   Serial.println("Render status bar");
   RenderStatusBar(buffer, jsonDoc, *timeinfo);
   Serial.println("Render main section");
   RenderMainSection(buffer, jsonDoc, *timeinfo, history);
   Serial.println("Render dailies");
   RenderDailys(buffer, jsonDoc, *timeinfo);

   return true;
}
