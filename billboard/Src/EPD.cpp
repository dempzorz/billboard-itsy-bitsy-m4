
#include "EPD.h"

#include "ImageUtils.h"
#include "WeatherIcons.h"

#include "Images/sf_pixelate_regular_21.h"

#include <stdlib.h>
#include <SPI.h>

namespace {
   constexpr uint8_t TestPin = 13;
}

Epd::Epd(uint8_t resetPin, uint8_t dataCommandPin, uint8_t chipSelectPin, uint8_t busyPin) :
   m_resetPin(resetPin),
   m_dataCommandPin(dataCommandPin),
   m_chipSelectPin(chipSelectPin),
   m_busyPin(busyPin),
   m_displayImage(m_buffer, EPD7IN5B::Dimentions::width, EPD7IN5B::Dimentions::height)
{
   /*reset_pin = RST_PIN;
   dc_pin = DC_PIN;
   cs_pin = CS_PIN;
   busy_pin = BUSY_PIN;
   width = EPD_WIDTH;
   height = EPD_HEIGHT;*/
};

void Epd::PrintTest() {

   DigitalWrite(TestPin, LOW);
   DelayMs(100);
   DigitalWrite(TestPin, HIGH);
   DelayMs(100);
   DigitalWrite(TestPin, LOW);
   DelayMs(100);
   DigitalWrite(TestPin, HIGH);
   DelayMs(100);
   DigitalWrite(TestPin, LOW);
   DelayMs(100);
   DigitalWrite(TestPin, HIGH);
   DelayMs(100);
   DigitalWrite(TestPin, LOW);
   DelayMs(100);
   DigitalWrite(TestPin, HIGH);
   DelayMs(500);
}

bool Epd::Init() {
   Serial.println("Initialising epd 1");
   pinMode(m_chipSelectPin, OUTPUT);
   pinMode(m_resetPin, OUTPUT);
   pinMode(m_busyPin, INPUT);
   pinMode(m_dataCommandPin, OUTPUT);
   pinMode(TestPin, OUTPUT);
   Serial.println("Initialising epd 2");
   SPI.begin();
   Serial.println("Initialising epd 3");

   
   SPI.beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE0));
   //SPI.beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE0));
   Serial.println("Initialising epd 4");

   PrintTest();

   Reset();

   PrintTest();

   Serial.println("Initialising epd 5");

   SendCommand(EPD7IN5B::Commands::PowerSetting::Id);
   SendData(0x37);
   SendData(0x00);

   PrintTest();


   Serial.println("Initialising epd 6");

   SendCommand(EPD7IN5B::Commands::PanelSetting::Id);
   SendData(0xCF);
   SendData(0x08);
   Serial.println("Initialising epd 7");
   PrintTest();


   SendCommand(EPD7IN5B::Commands::BoosterSoftStart);
   SendData(0xc7);
   SendData(0xcc);
   SendData(0x28);
   //SendData(0x215);
   Serial.println("Initialising epd 8");
   PrintTest();


   SendCommand(EPD7IN5B::Commands::PowerOn);
   WaitUntilIdle();

   Serial.println("Initialising epd 8a");
   PrintTest();


   SendCommand(EPD7IN5B::Commands::PLLControl);
   SendData(0x3c);
   //SendData(0x3a); // ?

   Serial.println("Initialising epd 9");
PrintTest();


   SendCommand(EPD7IN5B::Commands::Temperature::Calibration);
   SendData(0x00);

   SendCommand(EPD7IN5B::Commands::VComAndDataIntervalSettings);
   SendData(0x77);

   SendCommand(EPD7IN5B::Commands::TConSetting);
   SendData(0x22);

   Serial.println("Initialising epd 10");

   SendCommand(EPD7IN5B::Commands::TConResolution);
   SendData(EPD7IN5B::Dimentions::width >> 8);
   SendData(EPD7IN5B::Dimentions::width & 0xFF);
   SendData(EPD7IN5B::Dimentions::height >> 8);
   SendData(EPD7IN5B::Dimentions::height & 0xFF);

   SendCommand(EPD7IN5B::Commands::VComDCSettings);
   SendData(0x1E);      //decide by LUT file
   //SendData(0x28);

   //SendCommand(EPD7IN5B::Commands::SPIFlashControl);
   //SendData(0x00);

   Serial.println("Initialising epd 11");
   SendCommand(EPD7IN5B::Commands::FlashMode);           //FLASH MODE            
   SendData(0x03);

   Serial.println("Initialising epd 12");

      Serial.println("Stopped");


   return true;
}

/**
 *  @brief: basic function for sending commands
 */
void Epd::SendCommand(unsigned char command) {
   DigitalWrite(m_dataCommandPin, LOW);
   SpiTransfer(command);
}

/**
 *  @brief: basic function for sending data
 */
void Epd::SendData(unsigned char data) {
   DigitalWrite(m_dataCommandPin, HIGH);
   SpiTransfer(data);
}

/**
 *  @brief: Wait until the busy_pin goes HIGH
 */
void Epd::WaitUntilIdle(void) {
   while (DigitalRead(m_busyPin) == 0) {      //0: busy, 1: idle
      DelayMs(100);
   }
}

/**
 *  @brief: module reset.
 *          often used to awaken the module in deep sleep,
 *          see Epd::Sleep();
 */
void Epd::Reset(void) {
   DigitalWrite(m_resetPin, LOW);                //module reset    
   DelayMs(200);
   DigitalWrite(m_resetPin, HIGH);
   DelayMs(200);
}

void Epd::DisplayFrame(const unsigned char** image_data) {
   auto loader = SmallFont::SmallFontLoader();

   unsigned char temp1, temp2;
   Serial.println("Start transmission");

   SendCommand(EPD7IN5B::Commands::DataStartTransmission1);
   
   Serial.println("Writing display buffer");
   for (size_t i = 0; i < BufferSize; ++i)
   {
      SendData(m_buffer[i]);
   }

   Serial.println("data set");

   //Serial.println("power on");
   //SendCommand(EPD7IN5B::Commands::PowerOn);
   //Serial.println("waiting for idle");
   //WaitUntilIdle();

   // Needed?
   //SendCommand(EPD7IN5B::Commands::DataStop);
   //SendData(0x33);

   Serial.println("refresh");

   SendCommand(EPD7IN5B::Commands::DisplayRefresh);
   DelayMs(100);
   Serial.println("waiting for idle");
   WaitUntilIdle();
}

/**
 *  @brief: After this command is transmitted, the chip would enter the
 *          deep-sleep mode to save power.
 *          The deep sleep mode would return to standby by hardware reset.
 *          The only one parameter is a check code, the command would be
 *          executed if check code = 0xA5.
 *          You can use EPD_Reset() to awaken
 */
void Epd::Sleep(void) {
   SendCommand(EPD7IN5B::Commands::PowerOff);
   WaitUntilIdle();
   SendCommand(EPD7IN5B::Commands::DeepSleep);
   SendData(0xa5);
}

Buffer& Epd::imageBuffer()
{
   return m_displayImage;
}

void Epd::DigitalWrite(int pin, int value) {
   digitalWrite(pin, value);
}

int Epd::DigitalRead(int pin) {
   return digitalRead(pin);
}

void Epd::DelayMs(unsigned int delaytime) {
   delay(delaytime);
}

void Epd::SpiTransfer(unsigned char data) {
   digitalWrite(m_chipSelectPin, LOW);
   SPI.transfer(data);
   digitalWrite(m_chipSelectPin, HIGH);
}
