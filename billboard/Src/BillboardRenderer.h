
#pragma once

#include "Images/Image.h"
#include "CircularBuffer.h"

#include <ArduinoJson.h>

struct HistoryItem
{
   HistoryItem() :
      hour(0),
      temperature(0.0f)
   {
   }

   HistoryItem(int hour, float temp) :
      hour(hour),
      temperature(temp)
   {
   }

   int hour = 0;
   float temperature = 0.0f;
};

namespace BillboardRenderer
{
   using HistoryBuffer = CircularBuffer<24, HistoryItem>;
   bool Render(Buffer& buffer, const JsonDocument& jsonDoc, HistoryBuffer& history);

   bool RenderTest(Buffer& buffer);
}
