
#pragma once

#include "Geometry.h"
#include "HardwareDefinitions.h"

#include "Images/Image.h"

#include <ArduinoJson.h>

namespace ImageUtils
{
   enum Color : uint8_t
   {
      Black = 0b000,
      White = 0b011,
      Grey = 0b001,
      Yellow = 0b100
   };

   constexpr inline void SetColour(Buffer& destination, size_t pixelColumn, size_t pixelRow, Color color)
   {
      size_t bufferIndex = ((pixelRow * ((destination.Width() + 1) / 2)) + (pixelColumn / 2));

      uint8_t mask = 0;
      uint8_t value = 0;

      if (pixelColumn & 1 == 1)
      {
         mask = 0xF;
         value = color;
      }
      else
      {
         mask = 0xF0;
         value = color << 4;
      }

      destination.Data()[bufferIndex] = (destination.Data()[bufferIndex] & ~mask) | (value & mask);
   }

   constexpr inline void FillRegion(Buffer& destination, const Region& region, Color color)
   {
      for (size_t row = region.topLeft.y; row < region.bottomRight.y; ++row)
      {
         for (size_t column = region.topLeft.x; column < region.bottomRight.x; ++column)
         {
            SetColour(destination, column, row, color);
         }
      }
   }

   constexpr inline void WriteGlyph(Buffer& destination, const Glyph& image, size_t x, size_t y)
   {
      int xStart = x + image.displayXOffset;
      int yStart = y + image.displayYOffset;

      for (size_t imageYPos = 0; imageYPos < image.glyphHeight; ++imageYPos)
      {
         for (size_t imageXPos = 0; imageXPos < image.glyphWidth; ++imageXPos)
         {
            size_t destinationIndex = (((yStart + imageYPos) * ((destination.Width() + 1) / 2)) + ((imageXPos + xStart) / 2));
            size_t imageIndex = (imageYPos * ((image.glyphWidth + 1) / 2)) + ((imageXPos) / 2);

            uint8_t result = 0;

            if ((imageXPos & 1) == 0)
            {
               result = (image.Data()[imageIndex] & 0xF0) >> 4;
               
            }
            else
            {
               result = (image.Data()[imageIndex] & 0x0F);
            }

            if (((imageXPos + xStart) & 1) == 0)
            {
               destination.Data()[destinationIndex] = (result << 4) | (destination.Data()[destinationIndex] & 0x0F);
            }
            else
            {
               destination.Data()[destinationIndex] = (result & 0x0F) | (destination.Data()[destinationIndex] & 0xF0);
            }
         }
      }
   }

   template<typename T>
   constexpr inline void WriteString(const char* text, Buffer& destination, const GlyphLoader<T>& font, size_t x, size_t y)
   {
      size_t currentPosX = x;

      while (*text != '\0')
      {
         // For some reason the string gets 194 chars in it.  Not sure how, but ignore them.
         if ((int)(*text) != 194) {
            const Glyph& glyph = font.GetGlyph(*text);
            WriteGlyph(destination, glyph, currentPosX, y);
            currentPosX += glyph.displayWidth;
         }

         text++;
      }
   }

   template<typename T>
   constexpr inline size_t StringWidth(const char* text, const GlyphLoader<T>& font)
   {
      size_t currentWidth = 0;

      while (*text != '\0')
      {
         const Glyph& glyph = font.GetGlyph(*text);
         currentWidth += glyph.displayWidth;
         text++;
      }

      return currentWidth;
   }

   template<typename T>
   constexpr inline size_t StringHeight(const char* text, const GlyphLoader<T>& font)
   {
      size_t maxHeight = 0;

      while (*text != '\0')
      {
         const Glyph& glyph = font.GetGlyph(*text);
         maxHeight = std::max(maxHeight, glyph.Height());
         text++;
      }

      return maxHeight;
   }

}