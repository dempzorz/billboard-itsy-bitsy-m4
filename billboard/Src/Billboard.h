#pragma once

#include "AirLiftBitsty.h"
#include "BillboardRenderer.h"
#include "CircularBuffer.h"
#include "EPD.h"

class Billboard
{
public:
   Billboard();

   bool Init();
   bool Update();
   void Sleep(int millisecondsToSleep);

private:
   Epd m_epd;
   AirLiftBitsty m_airlift;

   BillboardRenderer::HistoryBuffer m_history;
};