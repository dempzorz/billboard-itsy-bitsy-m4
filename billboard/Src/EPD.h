
#pragma once

#include "Definitions.h"
#include "Geometry.h"
#include "HardwareDefinitions.h"

#include "Images/Image.h"

#include <stdlib.h>

class Epd 
{
public:
   Epd(uint8_t resetPin, uint8_t dataCommandPin, uint8_t chipSelectPin, uint8_t busyPin);

   bool Init();
   void WaitUntilIdle();
   void Reset();
   void DisplayFrame(const unsigned char** image_data);
   void SendCommand(unsigned char command);
   void SendData(unsigned char data);
   void Sleep();

   Buffer& imageBuffer();


private:
   
   void DigitalWrite(int pin, int value);
   int DigitalRead(int pin);
   void DelayMs(unsigned int delaytime);
   void SpiTransfer(unsigned char data);
   void PrintTest();
   
   uint8_t m_resetPin;
   uint8_t m_dataCommandPin;
   uint8_t m_chipSelectPin;
   uint8_t m_busyPin;

   constexpr static size_t BufferSize = (EPD7IN5B::Dimentions::width * EPD7IN5B::Dimentions::height) / 2;
   uint8_t m_buffer[BufferSize];
   Buffer m_displayImage;
};