
#include "AirLiftBitsty.h"

namespace
{
#define SPIWIFI       SPI  // The SPI port
#define SPIWIFI_SS    13   // Chip select pin
#define ESP32_RESETN  12   // Reset pin
#define SPIWIFI_ACK   11   // a.k.a BUSY or READY pin
#define ESP32_GPIO0   -1

   constexpr size_t bufferSize = 30000;
   
}


AirLiftBitsty::AirLiftBitsty(uint8_t chipSelectPin, uint8_t resetPin, uint8_t busyPin) :
   m_resetPin(resetPin),
   m_chipSelectPin(chipSelectPin),
   m_busyPin(busyPin)
{

}

AirLiftBitsty::~AirLiftBitsty()
{
   m_client.stop();
   WiFi.disconnect();
}

bool AirLiftBitsty::Init()
{
   WiFi.setPins(SPIWIFI_SS, SPIWIFI_ACK, ESP32_RESETN, ESP32_GPIO0, &SPIWIFI);
   return true;
}

bool AirLiftBitsty::IsReady() const
{
   constexpr int MaxAttempts = 65;
   int attempt = 0;

   while (WiFi.status() == WL_NO_MODULE && attempt < MaxAttempts)
   {
      Serial.println("Communication with WiFi module failed!");
      delay(1000);
      attempt++;
   }

   return WiFi.status() != WL_NO_MODULE;
}

bool AirLiftBitsty::ConnectToNetwork(const char* ssid, const char* password)
{
   Serial.print("Attempting to connect to SSID: ");
   Serial.println(ssid);

   constexpr int MaxAttempts = 65;

   int status = WL_DISCONNECTED;

   for (int i = 0; i < MaxAttempts && status != WL_CONNECTED; ++i)
   {
      status = WiFi.begin(ssid, password);

      Serial.print("Connection Status: ");
      Serial.println(status);

      // wait 3 seconds for connection:
      delay(3000);
   }

   m_client.setTimeout(5000);

   return status == WL_CONNECTED;
}

void AirLiftBitsty::DisconnectFromNetwork()
{
   Serial.println("Disconnecting from AP");
   m_client.flush();
   m_client.stop();
   WiFi.end();
   Serial.println("Disconnected from AP");
}

bool AirLiftBitsty::MakeWebRequest(const char* server, const char* path, uint16_t port)
{
   constexpr int MaxAttempts = 65;
   int currentTry = 0;

   m_client.flush();
   m_client.stop();

   while (!m_client.connect(server, port))
   {
      delay(300);

      Serial.println("Failed to connect to server");
      Serial.print("Network status:");
      Serial.println(WiFi.status());

      if (currentTry++ == MaxAttempts)
      {
         return false;
      }
   }

   // send the HTTP GET request:
   m_client.print("GET ");
   m_client.print(path);
   m_client.println(" HTTP/1.1");
    
   m_client.print("Host: ");
   m_client.println(server);
   m_client.println("User-Agent: ArduinoWiFi/1.1");
   m_client.println("Connection: close");
   m_client.println();

   return true;
}

bool AirLiftBitsty::SendData(const char* data, size_t size)
{
   Serial.println("Sending data");
   for (size_t i = 0; i < size; ++i)
   {
      m_client.print(data[i]);
      Serial.println((int)data[i]);
   }
   Serial.println("Data sent");
   return true;
}

bool AirLiftBitsty::IsConnectedToNetwork()
{
   return WiFi.status() == WL_CONNECTED;
}

bool AirLiftBitsty::IsConnectedToServer()
{
   return m_client.connected() == 1;
}

bool AirLiftBitsty::WaitForMessage(ArduinoJson::DynamicJsonDocument& doc, char* buffer, size_t bufferSize)
{
   Serial.println("Waiting for message ");

   delay(1000);

   size_t receivedBytes = 0;
   
   while (receivedBytes < bufferSize)
   {
      Serial.println("Getting bytes: ");
      receivedBytes += m_client.readBytes(buffer + receivedBytes, bufferSize - receivedBytes);
      Serial.print("Received bytes: ");
      Serial.println(receivedBytes);

      if (receivedBytes > 4 && 
         buffer[receivedBytes - 4] == '\r' &&
         buffer[receivedBytes - 3] == '\n' &&
         buffer[receivedBytes - 2] == '\r' &&
         buffer[receivedBytes - 1] == '\n')
      {
         Serial.println("Got full message ");

         break;
      }

      delay(100);
   }

   Serial.println("Buffer start: ***************************************************");
   for (size_t i = 0; i < receivedBytes; ++i)
   {
      Serial.print(buffer[i]);
   }
   Serial.println("Buffer end: ***************************************************");

   size_t dataStartByte = receivedBytes;

   // Skip the headers
   for (size_t i = 0; i < receivedBytes; ++i)
   {
      if (i < receivedBytes - 4 &&
         buffer[i] == '\r' &&
         buffer[i + 1] == '\n' &&
         buffer[i + 2] == '\r' &&
         buffer[i + 3] == '\n')
      {
         dataStartByte = i + 4;

         break;
      }
   }

   DeserializationError error = deserializeJson(doc, buffer + dataStartByte);
   if (error) {
      Serial.print(F("deserializeJson() failed: "));
      Serial.println(error.c_str());
      return false;
   }

   //result = doc.as<JsonObject>();
   return true;
}

IPAddress AirLiftBitsty::LocalIpAddress() const
{
   return WiFi.localIP();
}
