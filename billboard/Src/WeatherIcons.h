#pragma once

#include "Images/Image.h"
#include "Images/weather_icons.h"

#include "Geometry.h"
#include "ImageUtils.h"

#include <array>

namespace Weather
{
   enum class Icon : uint8_t
   {
      Cloudy,
      Clear,
      Fog,
      HeavyRain,
      HeavySnow,
      HeavyThunder,
      HeavyRainWithThunder,
      LightRain,
      LightSnow,
      LightThunder,
      LightRainWithThunder,
      Mist,
      Overcast,
      PartialCloud,
      Sleet,
      //SnowThunder,
      Wind
   };

   enum class TimeOfDay
   {
      Day,
      Night
   };

   enum class Size : uint8_t
   {
      Large,
      Small
   };

   const Glyph& GetIcon(Icon icon, TimeOfDay timeOfDay, Size size);
   
   const Glyph& GetIconFromOpenWeatherId(int id, TimeOfDay timeOfDay, Size size);

}