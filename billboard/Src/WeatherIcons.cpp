
#include "WeatherIcons.h"

#include "Geometry.h"
#include "ImageUtils.h"

#include <array>
#include <iterator>

using namespace Weather;

namespace
{
   constexpr size_t BufferSize(size_t width, size_t height)
   {
      return ((width + 1) / 2 * height);
   }

   template<size_t ResultWidth, size_t ResultHeight, size_t SubImageSize>
   constexpr ArrayImage<ResultWidth, ResultHeight> CreateComposite(Glyph baseImage, size_t subImageCenterY, std::array<Glyph, SubImageSize> subImages)
   {
      // TODO: IOs this really why I need ArrayImag??

      std::array<uint8_t, ArrayImage<ResultWidth, ResultHeight>::bufferSize> data{ 0 };
      Buffer buffer(data.data(), ResultWidth, ResultHeight);

      for (size_t i = 0; i < data.size(); ++i)
      {
         if (i < baseImage.DataLenth())
         {
            data[i] = *(baseImage.data + i);
         }
         else
         {
            data[i] = (ImageUtils::White << 4) | ImageUtils::White;
         }
      }

      Region kRegionToClear(baseImage.displayWidth / 2 - baseImage.displayWidth / 4, baseImage.glyphHeight / 4 * 3,
         baseImage.displayWidth / 2 + baseImage.displayWidth / 4, baseImage.glyphHeight);

      ImageUtils::FillRegion(buffer, kRegionToClear, ImageUtils::White);

      constexpr size_t margin = 3;

      size_t subRegionWidth = (kRegionToClear.Width() - margin * 2) / (subImages.size());

      size_t currentXOffset = kRegionToClear.topLeft.x + margin + subRegionWidth / 2;


      for (size_t i = 0; i < subImages.size(); ++i)
      {
         const Glyph& subImage = subImages[i];
         size_t yPos = subImageCenterY - subImage.displayWidth / 2;
         ImageUtils::WriteGlyph(buffer, subImages[i], currentXOffset - subImage.displayWidth / 2, yPos);
         currentXOffset += subRegionWidth;
      }

      return ArrayImage<ResultWidth, ResultHeight>(std::move(data));
   }

   template<size_t ResultWidth, size_t ResultHeight, size_t SubImageSize>
   constexpr ArrayImage<ResultWidth, ResultHeight> Testing(Glyph baseImage, size_t subImageCenterY, std::array<Glyph, SubImageSize> subImages)
   {
      std::array<uint8_t, ArrayImage<ResultWidth, ResultHeight>::bufferSize> data{ 0 };

      Buffer buffer(data.data(), ResultWidth, ResultHeight);

      for (size_t i = 0; i < data.size(); ++i)
      {
         if (i < baseImage.DataLenth())
         {
            data[i] = *(baseImage.data + i);
         }
         else
         {
            data[i] = (ImageUtils::White << 4) | ImageUtils::White;
         }
      }

      Region kRegionToClear(baseImage.displayWidth / 2 - baseImage.displayWidth / 4, baseImage.glyphHeight / 4 * 3,
         baseImage.displayWidth / 2 + baseImage.displayWidth / 4, baseImage.glyphHeight);

      ImageUtils::FillRegion(buffer, kRegionToClear, ImageUtils::White);

            constexpr size_t margin = 3;

      size_t subRegionWidth = (kRegionToClear.Width() - margin * 2) / (subImages.size());

      size_t currentXOffset = kRegionToClear.topLeft.x + margin + subRegionWidth / 2;


      for (size_t i = 0; i < subImages.size(); ++i)
      {
         const Glyph& subImage = subImages[i];
         size_t yPos = subImageCenterY - subImage.displayWidth / 2;
         ImageUtils::WriteGlyph(buffer, subImages[i], currentXOffset - subImage.displayWidth / 2, yPos);
         currentXOffset += subRegionWidth;
      }

      return ArrayImage<ResultWidth, ResultHeight>(std::move(data));

      return ArrayImage<ResultWidth, ResultHeight>(std::move(data));
   }

   using namespace Icons;

   constexpr size_t LargeSunMoonSubImageCenterY = 130;
   constexpr size_t LargeCloudSubImageCenterY = 90;
   constexpr size_t SmallSunMoonSubImageCenterY = 55;
   constexpr size_t SmallCloudSubImageCenterY = 35;

   // Large Night Lightning Rain
   constexpr std::array<Glyph, 3> LargeRainThunderSubImages = { largeRainDropImage, largeSingleLightningImage, largeRainDropImage };
   using LargeNightRainThunderArrayImage = ArrayImage<largeCloudsWithMoonsImage.displayWidth, largeCloudsWithMoonsImage.glyphHeight + 50>;
   constexpr LargeNightRainThunderArrayImage test = Testing<LargeNightRainThunderArrayImage::width, LargeNightRainThunderArrayImage::height>(largeCloudsWithMoonsImage, LargeSunMoonSubImageCenterY, LargeRainThunderSubImages);

   constexpr LargeNightRainThunderArrayImage LargeNightRainThunderArray = CreateComposite<LargeNightRainThunderArrayImage::width, LargeNightRainThunderArrayImage::height>(largeCloudsWithMoonsImage, LargeSunMoonSubImageCenterY, LargeRainThunderSubImages);
   constexpr Glyph LargeNightRainThunderImage(LargeNightRainThunderArray);

   // Large Night Rain
   constexpr std::array<Glyph, 2> LargeRainSubImages = { largeRainDropImage, largeRainDropImage };
   using LargeNightRainArrayImage = ArrayImage<largeCloudsWithMoonsImage.displayWidth, largeCloudsWithMoonsImage.glyphHeight + 20>;
   constexpr LargeNightRainArrayImage LargeNightRainArray = CreateComposite<LargeNightRainArrayImage::width, LargeNightRainArrayImage::height>(largeCloudsWithMoonsImage, LargeSunMoonSubImageCenterY, LargeRainSubImages);
   constexpr Glyph LargeNightRainImage(LargeNightRainArray);

   // Large Night Sleet
   constexpr std::array<Glyph, 2> LargeSleetSubImages = { largeSnowImage, largeRainDropImage };
   using LargeNightSleetArrayImage = ArrayImage<largeCloudsWithMoonsImage.displayWidth, largeCloudsWithMoonsImage.glyphHeight + 20>;
   constexpr LargeNightSleetArrayImage LargeNightSleetArray = CreateComposite<LargeNightSleetArrayImage::width, LargeNightSleetArrayImage::height>(largeCloudsWithMoonsImage, LargeSunMoonSubImageCenterY, LargeSleetSubImages);
   constexpr Glyph LargeNightSleetImage(LargeNightSleetArray);

   // Large Night Snow
   constexpr std::array<Glyph, 2> LargeSnowSubImages = { largeSnowImage, largeSnowImage };
   using LargeNightSnowArrayImage = ArrayImage<largeCloudsWithMoonsImage.displayWidth, largeCloudsWithMoonsImage.glyphHeight + 20>;
   constexpr LargeNightSnowArrayImage LargeNightSnowArray = CreateComposite<LargeNightSnowArrayImage::width, LargeNightSnowArrayImage::height>(largeCloudsWithMoonsImage, LargeSunMoonSubImageCenterY, LargeSnowSubImages);
   constexpr Glyph LargeNightSnowImage(LargeNightSnowArray);

   // Large Night Thunder Snow
   constexpr std::array<Glyph, 3> LargeSnowThunderSubImages = { largeSnowImage, largeSingleLightningImage, largeSnowImage };
   using LargeNightSnowThunderArrayImage = ArrayImage<largeCloudsWithMoonsImage.displayWidth, largeCloudsWithMoonsImage.glyphHeight + 50>;
   constexpr LargeNightSnowThunderArrayImage LargeNightSnowThunderArray = CreateComposite<LargeNightSnowThunderArrayImage::width, LargeNightSnowThunderArrayImage::height>(largeCloudsWithMoonsImage, LargeSunMoonSubImageCenterY, LargeSnowThunderSubImages);
   constexpr Glyph LargeNightSnowThunderImage(LargeNightSnowThunderArray);

   // Large Fog
   constexpr std::array<Glyph, 1> LargeFogSubImages = { largeFogImage };
   using LargeFogArrayImage = ArrayImage<largeCloudsImage.displayWidth, largeCloudsImage.glyphHeight + 50>;
   constexpr LargeFogArrayImage LargeFogArray = CreateComposite<LargeFogArrayImage::width, LargeFogArrayImage::height>(largeCloudsWithMoonsImage, LargeCloudSubImageCenterY, LargeFogSubImages);
   constexpr Glyph LargeFogImage(LargeFogArray);
   
   // Large Mist
   constexpr std::array<Glyph, 1> LargeMistSubImages = { largeMistImage };
   using LargeMistArrayImage = ArrayImage<largeCloudsImage.displayWidth, largeCloudsImage.glyphHeight + 50>;
   constexpr LargeMistArrayImage LargeMistArray = CreateComposite<LargeMistArrayImage::width, LargeMistArrayImage::height>(largeCloudsWithMoonsImage, LargeCloudSubImageCenterY, LargeMistSubImages);
   constexpr Glyph LargeMistImage(LargeMistArray);

   // Large Day Lightning Rain
   using LargeDayRainThunderArrayImage = ArrayImage<largeCloudsWithSunImage.displayWidth, largeCloudsWithSunImage.glyphHeight + 50>;
   constexpr LargeDayRainThunderArrayImage LargeDayRainThunderArray = CreateComposite<LargeDayRainThunderArrayImage::width, LargeDayRainThunderArrayImage::height>(largeCloudsWithSunImage, LargeSunMoonSubImageCenterY, LargeRainThunderSubImages);
   constexpr Glyph LargeDayRainThunderImage(LargeDayRainThunderArray);

   // Large Day Rain
   using LargeDayRainArrayImage = ArrayImage<largeCloudsWithSunImage.displayWidth, largeCloudsWithSunImage.glyphHeight + 20>;
   constexpr LargeDayRainArrayImage LargeDayRainArray = CreateComposite<LargeDayRainArrayImage::width, LargeDayRainArrayImage::height>(largeCloudsWithSunImage, LargeSunMoonSubImageCenterY, LargeRainSubImages);
   constexpr Glyph LargeDayRainImage(LargeDayRainArray);

   // Large Day Sleet
   using LargeDaySleetArrayImage = ArrayImage<largeCloudsWithSunImage.displayWidth, largeCloudsWithSunImage.glyphHeight + 20>;
   constexpr LargeDaySleetArrayImage LargeDaySleetArray = CreateComposite<LargeDaySleetArrayImage::width, LargeDaySleetArrayImage::height>(largeCloudsWithSunImage, LargeSunMoonSubImageCenterY, LargeSleetSubImages);
   constexpr Glyph LargeDaySleetImage(LargeDaySleetArray);

   // Large Day Snow
   using LargeDaySnowArrayImage = ArrayImage<largeCloudsWithSunImage.displayWidth, largeCloudsWithSunImage.glyphHeight + 20>;
   constexpr LargeDaySnowArrayImage LargeDaySnowArray = CreateComposite<LargeDaySnowArrayImage::width, LargeDaySnowArrayImage::height>(largeCloudsWithSunImage, LargeSunMoonSubImageCenterY, LargeSnowSubImages);
   constexpr Glyph LargeDaySnowImage(LargeDaySnowArray);
   
   // Large Day Thunder Snow
   /*using LargeDaySnowThunderArrayImage = ArrayImage<largeCloudsWithSunImage.displayWidth, largeCloudsWithSunImage.glyphHeight + 50>;
   constexpr LargeDaySnowThunderArrayImage LargeDaySnowThunderArray = CreateComposite<LargeDaySnowThunderArrayImage::width, LargeDaySnowThunderArrayImage::height>(largeCloudsWithSunImage, LargeSunMoonSubImageCenterY, LargeSnowThunderSubImages);
   constexpr Glyph LargeDaySnowThunderImage(LargeDaySnowThunderArray);*/

   // Large Lightning Rain
   using LargeThunderRainArrayImage = ArrayImage<largeCloudsImage.displayWidth, largeCloudsImage.glyphHeight + 50>;
   constexpr LargeThunderRainArrayImage LargeThunderRainArray = CreateComposite<LargeThunderRainArrayImage::width, LargeThunderRainArrayImage::height>(largeCloudsImage, LargeCloudSubImageCenterY, LargeRainThunderSubImages);
   constexpr Glyph LargeThunderRainImage(LargeThunderRainArray);

   // Large Lightning Single
   constexpr std::array<Glyph, 1> LargeSingleThunderSubImages = { largeSingleLightningImage };
   using LargeSingleThunderArrayImage = ArrayImage<largeCloudsImage.displayWidth, largeCloudsImage.glyphHeight + 50>;
   constexpr LargeSingleThunderArrayImage LargeSingleThunderArray = CreateComposite<LargeSingleThunderArrayImage::width, LargeSingleThunderArrayImage::height>(largeCloudsImage, LargeCloudSubImageCenterY, LargeSingleThunderSubImages);
   constexpr Glyph LargeSingleThunderImage(LargeSingleThunderArray);

   // Large Lightning Multiple
   constexpr std::array<Glyph, 1> LargeMultipleThunderSubImages = { largeMultipleLightningImage };
   using LargeMultipleThunderArrayImage = ArrayImage<largeCloudsImage.displayWidth, largeCloudsImage.glyphHeight + 50>;
   constexpr LargeMultipleThunderArrayImage LargeMultipleThunderArray = CreateComposite<LargeMultipleThunderArrayImage::width, LargeMultipleThunderArrayImage::height>(largeCloudsImage, LargeCloudSubImageCenterY, LargeMultipleThunderSubImages);
   constexpr Glyph LargeMultipleThunderImage(LargeMultipleThunderArray);

   // Large Rain
   using LargeRainArrayImage = ArrayImage<largeCloudsImage.displayWidth, largeCloudsImage.glyphHeight + 50>;
   constexpr LargeRainArrayImage LargeRainArray = CreateComposite<LargeRainArrayImage::width, LargeRainArrayImage::height>(largeCloudsImage, LargeCloudSubImageCenterY, LargeRainSubImages);
   constexpr Glyph LargeRainImage(LargeRainArray);

   // Large Snow
   using LargeSnowArrayImage = ArrayImage<largeCloudsImage.displayWidth, largeCloudsImage.glyphHeight + 50>;
   constexpr LargeSnowArrayImage LargeSnowArray = CreateComposite<LargeSnowArrayImage::width, LargeSnowArrayImage::height>(largeCloudsImage, LargeCloudSubImageCenterY, LargeSnowSubImages);
   constexpr Glyph LargeSnowImage(LargeSnowArray);

   // Small Night Lightning Rain
   constexpr std::array<Glyph, 3> SmallRainThunderSubImages = { smallRainDropImage, smallSingleLightningImage, smallRainDropImage };
   using SmallNightRainThunderArrayImage = ArrayImage<smallCloudWithMoonImage.displayWidth, smallCloudWithMoonImage.glyphHeight + 15>;
   constexpr SmallNightRainThunderArrayImage SmallNightRainThunderArray = CreateComposite<SmallNightRainThunderArrayImage::width, SmallNightRainThunderArrayImage::height>(smallCloudWithMoonImage, SmallSunMoonSubImageCenterY, SmallRainThunderSubImages);
   constexpr Glyph SmallNightRainThunderImage(SmallNightRainThunderArray);

   // Small Night Rain
   constexpr std::array<Glyph, 2> SmallRainSubImages = { smallRainDropImage, smallRainDropImage };
   using SmallNightRainArrayImage = ArrayImage<smallCloudWithMoonImage.displayWidth, smallCloudWithMoonImage.glyphHeight + 5>;
   constexpr SmallNightRainArrayImage SmallNightRainArray = CreateComposite<SmallNightRainArrayImage::width, SmallNightRainArrayImage::height>(smallCloudWithMoonImage, SmallSunMoonSubImageCenterY, SmallRainSubImages);
   constexpr Glyph SmallNightRainImage(SmallNightRainArray);

   // Small Night Sleet
   constexpr std::array<Glyph, 2> SmallSleetSubImages = { smallSnowImage, smallRainDropImage };
   using SmallNightSleetArrayImage = ArrayImage<smallCloudWithMoonImage.displayWidth, smallCloudWithMoonImage.glyphHeight + 5>;
   constexpr SmallNightSleetArrayImage SmallNightSleetArray = CreateComposite<SmallNightSleetArrayImage::width, SmallNightSleetArrayImage::height>(smallCloudWithMoonImage, SmallSunMoonSubImageCenterY, SmallSleetSubImages);
   constexpr Glyph SmallNightSleetImage(SmallNightSleetArray);

   // Small Night Snow
   constexpr std::array<Glyph, 2> SmallSnowSubImages = { smallSnowImage, smallSnowImage };
   using SmallNightSnowArrayImage = ArrayImage<smallCloudWithMoonImage.displayWidth, smallCloudWithMoonImage.glyphHeight + 5>;
   constexpr SmallNightSnowArrayImage SmallNightSnowArray = CreateComposite<SmallNightSnowArrayImage::width, SmallNightSnowArrayImage::height>(smallCloudWithMoonImage, SmallSunMoonSubImageCenterY, SmallSnowSubImages);
   constexpr Glyph SmallNightSnowImage(SmallNightSnowArray);

   // Small Night Thunder Snow
   constexpr std::array<Glyph, 3> SmallSnowThunderSubImages = { smallSnowImage, smallSingleLightningImage, smallSnowImage };
   using SmallNightSnowThunderArrayImage = ArrayImage<smallCloudWithMoonImage.displayWidth, smallCloudWithMoonImage.glyphHeight + 15>;
   constexpr SmallNightSnowThunderArrayImage SmallNightSnowThunderArray = CreateComposite<SmallNightSnowThunderArrayImage::width, SmallNightSnowThunderArrayImage::height>(smallCloudWithMoonImage, SmallSunMoonSubImageCenterY, SmallSnowThunderSubImages);
   constexpr Glyph SmallNightSnowThunderImage(SmallNightSnowThunderArray);

   // Small Fog
   constexpr std::array<Glyph, 1> SmallFogSubImages = { smallFogImage };
   using SmallFogArrayImage = ArrayImage<smallCloudImage.displayWidth, smallCloudImage.glyphHeight + 20>;
   constexpr SmallFogArrayImage SmallFogArray = CreateComposite<SmallFogArrayImage::width, SmallFogArrayImage::height>(smallCloudWithMoonImage, SmallCloudSubImageCenterY, SmallFogSubImages);
   constexpr Glyph SmallFogImage(SmallFogArray);

   // Small Mist
   constexpr std::array<Glyph, 1> SmallMistSubImages = { smallMistImage };
   using SmallMistArrayImage = ArrayImage<smallCloudImage.displayWidth, smallCloudImage.glyphHeight + 20>;
   constexpr SmallMistArrayImage SmallMistArray = CreateComposite<SmallMistArrayImage::width, SmallMistArrayImage::height>(smallCloudWithMoonImage, SmallCloudSubImageCenterY, SmallMistSubImages);
   constexpr Glyph SmallMistImage(SmallMistArray);

   // Small Day Lightning Rain
   using SmallDayRainThunderArrayImage = ArrayImage<smallCloudWithSunImage.displayWidth, smallCloudWithSunImage.glyphHeight + 15>;
   constexpr SmallDayRainThunderArrayImage SmallDayRainThunderArray = CreateComposite<SmallDayRainThunderArrayImage::width, SmallDayRainThunderArrayImage::height>(smallCloudWithSunImage, SmallSunMoonSubImageCenterY, SmallRainThunderSubImages);
   constexpr Glyph SmallDayRainThunderImage(SmallDayRainThunderArray);

   // Small Day Rain
   using SmallDayRainArrayImage = ArrayImage<smallCloudWithSunImage.displayWidth, smallCloudWithSunImage.glyphHeight + 5>;
   constexpr SmallDayRainArrayImage SmallDayRainArray = CreateComposite<SmallDayRainArrayImage::width, SmallDayRainArrayImage::height>(smallCloudWithSunImage, SmallSunMoonSubImageCenterY, SmallRainSubImages);
   constexpr Glyph SmallDayRainImage(SmallDayRainArray);

   // Small Day Sleet
   using SmallDaySleetArrayImage = ArrayImage<smallCloudWithSunImage.displayWidth, smallCloudWithSunImage.glyphHeight + 5>;
   constexpr SmallDaySleetArrayImage SmallDaySleetArray = CreateComposite<SmallDaySleetArrayImage::width, SmallDaySleetArrayImage::height>(smallCloudWithSunImage, SmallSunMoonSubImageCenterY, SmallSleetSubImages);
   constexpr Glyph SmallDaySleetImage(SmallDaySleetArray);

   // Small Day Snow
   using SmallDaySnowArrayImage = ArrayImage<smallCloudWithSunImage.displayWidth, smallCloudWithSunImage.glyphHeight + 5>;
   constexpr SmallDaySnowArrayImage SmallDaySnowArray = CreateComposite<SmallDaySnowArrayImage::width, SmallDaySnowArrayImage::height>(smallCloudWithSunImage, SmallSunMoonSubImageCenterY, SmallSnowSubImages);
   constexpr Glyph SmallDaySnowImage(SmallDaySnowArray);

   // Small Day Thunder Snow
   /*using SmallDaySnowThunderArrayImage = ArrayImage<smallCloudWithSunImage.displayWidth, smallCloudWithSunImage.glyphHeight + 50>;
   constexpr SmallDaySnowThunderArrayImage SmallDaySnowThunderArray = CreateComposite<SmallDaySnowThunderArrayImage::width, SmallDaySnowThunderArrayImage::height>(smallCloudWithSunImage, SmallSubImageCenterY, SmallSnowThunderSubImages);
   constexpr Glyph SmallDaySnowThunderImage(SmallDaySnowThunderArray);*/

   // Small Lightning Rain
   using SmallThunderRainArrayImage = ArrayImage<smallCloudImage.displayWidth, smallCloudImage.glyphHeight + 15>;
   constexpr SmallThunderRainArrayImage SmallThunderRainArray = CreateComposite<SmallThunderRainArrayImage::width, SmallThunderRainArrayImage::height>(smallCloudImage, SmallCloudSubImageCenterY, SmallRainThunderSubImages);
   constexpr Glyph SmallThunderRainImage(SmallThunderRainArray);

   // Small Lightning Single
   constexpr std::array<Glyph, 1> SmallSingleThunderSubImages = { smallSingleLightningImage };
   using SmallSingleThunderArrayImage = ArrayImage<smallCloudImage.displayWidth, smallCloudImage.glyphHeight + 15>;
   constexpr SmallSingleThunderArrayImage SmallSingleThunderArray = CreateComposite<SmallSingleThunderArrayImage::width, SmallSingleThunderArrayImage::height>(smallCloudImage, SmallCloudSubImageCenterY, SmallSingleThunderSubImages);
   constexpr Glyph SmallSingleThunderImage(SmallSingleThunderArray);

   // Small Lightning Multiple
   constexpr std::array<Glyph, 1> SmallMultipleThunderSubImages = { smallMultipleLightningImage };
   using SmallMultipleThunderArrayImage = ArrayImage<smallCloudImage.displayWidth, smallCloudImage.glyphHeight + 15>;
   constexpr SmallMultipleThunderArrayImage SmallMultipleThunderArray = CreateComposite<SmallMultipleThunderArrayImage::width, SmallMultipleThunderArrayImage::height>(smallCloudImage, SmallCloudSubImageCenterY, SmallMultipleThunderSubImages);
   constexpr Glyph SmallMultipleThunderImage(SmallMultipleThunderArray);

   // Small Rain
   using SmallRainArrayImage = ArrayImage<smallCloudImage.displayWidth, smallCloudImage.glyphHeight + 5>;
   constexpr SmallRainArrayImage SmallRainArray = CreateComposite<SmallRainArrayImage::width, SmallRainArrayImage::height>(smallCloudImage, SmallCloudSubImageCenterY, SmallRainSubImages);
   constexpr Glyph SmallRainImage(SmallRainArray);

   // Small Snow
   using SmallSnowArrayImage = ArrayImage<smallCloudImage.displayWidth, smallCloudImage.glyphHeight + 5>;
   constexpr SmallSnowArrayImage SmallSnowArray = CreateComposite<SmallSnowArrayImage::width, SmallSnowArrayImage::height>(smallCloudImage, SmallCloudSubImageCenterY, SmallSnowSubImages);
   constexpr Glyph SmallSnowImage(SmallSnowArray);
}

const Glyph& Weather::GetIcon(Icon icon, TimeOfDay timeOfDay, Size size)
{
   // These don't care about day\night
   switch (icon)
   {
   case Icon::Cloudy: return size == Size::Large ? Icons::largeCloudsImage : Icons::smallCloudImage;
   case Icon::Fog: return size == Size::Large ? LargeFogImage : SmallFogImage;
   case Icon::HeavyRainWithThunder: return size == Size::Large ? LargeThunderRainImage : SmallThunderRainImage;
   case Icon::HeavyRain: return size == Size::Large ? LargeRainImage : SmallRainImage;
   case Icon::HeavySnow: return size == Size::Large ? LargeSnowImage : SmallSnowImage;
   case Icon::HeavyThunder: return size == Size::Large ? LargeMultipleThunderImage : SmallMultipleThunderImage;
   case Icon::LightThunder: return size == Size::Large ? LargeSingleThunderImage : SmallSingleThunderImage;
   case Icon::Mist: return size == Size::Large ? LargeMistImage : SmallMistImage;
   case Icon::Overcast: return size == Size::Large ? Icons::largeCloudsImage : Icons::smallCloudImage;
      //case Icon::Wind: return size == Size::Large ? Icons:: : Icons::;
   }

   if (timeOfDay == TimeOfDay::Day)
   {
      switch (icon)
      {
      case Icon::Clear: return size == Size::Large ? Icons::largeSunImage : Icons::smallSunImage;
      case Icon::LightRainWithThunder: return size == Size::Large ? LargeDayRainThunderImage : SmallDayRainThunderImage;
      case Icon::PartialCloud: return size == Size::Large ? Icons::largeCloudsWithSunImage : Icons::smallCloudWithSunImage;
      case Icon::LightRain: return size == Size::Large ? LargeDayRainImage : SmallDayRainImage;
      case Icon::Sleet: return size == Size::Large ? LargeDaySleetImage : SmallDaySleetImage;
      case Icon::LightSnow: return size == Size::Large ? LargeDaySnowImage : SmallDaySnowImage;
     // case Icon::SnowThunder: return size == Size::Large ? LargeDaySnowThunderImage : SmallDaySnowThunderImage;
      }
   }
   else
   {
      switch (icon)
      {
      case Icon::Clear: return size == Size::Large ? Icons::largeMoonImage : Icons::smallMoonImage;
      case Icon::LightRainWithThunder: return size == Size::Large ? LargeNightRainThunderImage : SmallNightRainThunderImage;
      case Icon::PartialCloud: return size == Size::Large ? Icons::largeCloudsWithMoonsImage : Icons::smallCloudWithMoonImage;
      case Icon::LightRain: return size == Size::Large ? LargeNightRainImage : SmallNightRainImage;
      case Icon::Sleet: return size == Size::Large ? LargeNightRainThunderImage : SmallNightRainThunderImage;
      case Icon::LightSnow: return size == Size::Large ? LargeNightSnowImage : SmallNightSnowImage;
      //case Icon::SnowThunder: return size == Size::Large ? LargeNightSnowThunderImage : SmallNightSnowThunderImage;
      }
   }

   // Invalid
   abort();
}


const Glyph& Weather::GetIconFromOpenWeatherId(int id, TimeOfDay timeOfDay, Size size)
{
   switch (id)
   {
   case 200: return GetIcon(Icon::LightRainWithThunder, timeOfDay, size);	//thunderstorm with light rain
   case 201: return GetIcon(Icon::HeavyRainWithThunder, timeOfDay, size);	//thunderstorm with rain
   case 202: return GetIcon(Icon::HeavyRainWithThunder, timeOfDay, size);	//thunderstorm with heavy rain
   case 210: return GetIcon(Icon::LightThunder, timeOfDay, size);	//light thunderstorm
   case 211: return GetIcon(Icon::LightThunder, timeOfDay, size);	//thunderstorm
   case 212: return GetIcon(Icon::HeavyThunder, timeOfDay, size);	//heavy thunderstorm
   case 221: return GetIcon(Icon::LightThunder, timeOfDay, size);	//ragged thunderstorm
   case 230: return GetIcon(Icon::LightRainWithThunder, timeOfDay, size);	//thunderstorm with light drizzle
   case 231: return GetIcon(Icon::LightRainWithThunder, timeOfDay, size);	//thunderstorm with drizzle
   case 232: return GetIcon(Icon::LightRainWithThunder, timeOfDay, size);	//thunderstorm with heavy drizzle
   case 300: return GetIcon(Icon::LightRain, timeOfDay, size);	//light intensity drizzle
   case 301: return GetIcon(Icon::LightRain, timeOfDay, size);	//drizzle
   case 302: return GetIcon(Icon::LightRain, timeOfDay, size);	//heavy intensity drizzle
   case 310: return GetIcon(Icon::LightRain, timeOfDay, size);	//light intensity drizzle rain
   case 311: return GetIcon(Icon::LightRain, timeOfDay, size);	//drizzle rain
   case 312: return GetIcon(Icon::LightRain, timeOfDay, size);	//heavy intensity drizzle rain
   case 313: return GetIcon(Icon::LightRain, timeOfDay, size);	//shower rain and drizzle
   case 314: return GetIcon(Icon::LightRain, timeOfDay, size);	//heavy shower rain and drizzle
   case 321: return GetIcon(Icon::LightRain, timeOfDay, size);	//shower drizzle
   case 500: return GetIcon(Icon::LightRain, timeOfDay, size);	//light rain
   case 501: return GetIcon(Icon::HeavyRain, timeOfDay, size);	//moderate rain
   case 502: return GetIcon(Icon::HeavyRain, timeOfDay, size);	//heavy intensity rain
   case 503: return GetIcon(Icon::HeavyRain, timeOfDay, size);	//very heavy rain
   case 504: return GetIcon(Icon::HeavyRain, timeOfDay, size);	//extreme rain
   case 511: return GetIcon(Icon::Sleet, timeOfDay, size);	//freezing rain
   case 520: return GetIcon(Icon::LightRain, timeOfDay, size);	//light intensity shower rain
   case 521: return GetIcon(Icon::LightRain, timeOfDay, size);	//shower rain
   case 522: return GetIcon(Icon::LightRain, timeOfDay, size);	//heavy intensity shower rain
   case 531: return GetIcon(Icon::LightRain, timeOfDay, size);	//ragged shower rain
   case 600: return GetIcon(Icon::LightSnow, timeOfDay, size);	//light snow
   case 601: return GetIcon(Icon::HeavySnow, timeOfDay, size);	//Snow
   case 602: return GetIcon(Icon::HeavySnow, timeOfDay, size);	//Heavy snow
   case 611: return GetIcon(Icon::Sleet, timeOfDay, size);	//Sleet
   case 612: return GetIcon(Icon::Sleet, timeOfDay, size);	//Light shower sleet
   case 613: return GetIcon(Icon::Sleet, timeOfDay, size);	//Shower sleet
   case 615: return GetIcon(Icon::Sleet, timeOfDay, size);	//Light rain and snow
   case 616: return GetIcon(Icon::Sleet, timeOfDay, size);	//Rain and snow
   case 620: return GetIcon(Icon::Sleet, timeOfDay, size);	//Light shower snow
   case 621: return GetIcon(Icon::Sleet, timeOfDay, size);	//Shower snow
   case 622: return GetIcon(Icon::Sleet, timeOfDay, size);	//Heavy shower snow
   case 701: return GetIcon(Icon::Mist, timeOfDay, size);	//mist
   case 711: return GetIcon(Icon::Fog, timeOfDay, size);	//Smoke
   case 721: return GetIcon(Icon::Fog, timeOfDay, size);	//Haze
   case 731: return GetIcon(Icon::Fog, timeOfDay, size);	//sand/ dust whirls
   case 741: return GetIcon(Icon::Fog, timeOfDay, size);	//fog
   case 751: return GetIcon(Icon::Fog, timeOfDay, size);	//sand
   case 761: return GetIcon(Icon::Fog, timeOfDay, size);	//dust
   case 762: return GetIcon(Icon::Fog, timeOfDay, size);	//volcanic ash
   case 771: return GetIcon(Icon::Fog, timeOfDay, size);	//squalls
   case 781: return GetIcon(Icon::Fog, timeOfDay, size);	//tornado
   case 800: return GetIcon(Icon::Clear, timeOfDay, size);	//clear sky
   case 801: return GetIcon(Icon::Clear, timeOfDay, size);	//few clouds: 11-25%
   case 802: return GetIcon(Icon::PartialCloud, timeOfDay, size);	//scattered clouds: 25-50%
   case 803: return GetIcon(Icon::PartialCloud, timeOfDay, size);	//broken clouds: 51-84%
   case 804: return GetIcon(Icon::Cloudy, timeOfDay, size);	//overcast clouds: 85-100%
   }

   // temp
   return GetIcon(Icon::HeavySnow, timeOfDay, size);
}