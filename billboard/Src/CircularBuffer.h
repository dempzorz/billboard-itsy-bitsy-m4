
#pragma once

#include <array>
#include <functional>

template<size_t Size, typename Value>
class CircularBuffer
{
public:

   void Push(const Value& value)
   {
      m_data[m_front] = value;

      if (!m_isEmpty && m_front == m_back)
      {
         m_back = NextIndex(m_back);
      }

      m_isEmpty = false;

      m_front = NextIndex(m_front);
   }

   void PopBack()
   {
      if (!m_isEmpty)
      {
         m_back = NextIndex(m_back);
         m_isEmpty = m_back == m_front;
      }
   }

   const Value& PeekFront() const
   {
      return m_data[PrevIndex(m_front)];
   }

   void PopFront()
   {
      if (!m_isEmpty)
      {
         m_front = PrevIndex(m_front);
         m_isEmpty = m_back == m_front;
      }
   }

   bool IsEmpty() const
   {
      return m_isEmpty;
   }

   size_t Count() const
   {
      if (m_isEmpty) return 0;
      if (m_front == m_back) return Size;
      if (m_back > m_front) return m_front + (Size - m_back);
      return m_front - m_back;
   }

   void Iterate(size_t countToIterate, const std::function<void(const Value&)>& callback)
   {
      countToIterate = std::min(countToIterate, Count());

      if (countToIterate == 0) return;

      // The number from the back to start from
      size_t diffFromBack = Count() - countToIterate;

      size_t currentIndex = m_back;

      for (size_t i = 0; i < diffFromBack; ++i) {
         currentIndex = NextIndex(currentIndex);
      }
      
      for (size_t i = 0; i < countToIterate; ++i)
      {
         callback(m_data[currentIndex]);
         currentIndex = NextIndex(currentIndex);
      }
   }

private:

   constexpr static size_t NextIndex(size_t current)
   {
      return (current + 1) % Size;
   }

   constexpr static size_t PrevIndex(size_t current)
   {
      //return current == 0 ? Size - 1 : current - 1;

      if (current == 0)
      {
         return Size - 1;
      }

      return current - 1;
   }

   std::array<Value, Size> m_data;
   bool m_isEmpty = true;

   size_t m_front = 0; // The index to insert the next item
   size_t m_back = 0; // The index of the first item
};