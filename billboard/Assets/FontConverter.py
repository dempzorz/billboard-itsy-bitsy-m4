#!python

from PIL import Image

import json
import sys

import xml.etree.ElementTree as ET

if len(sys.argv) != 2:
    sys.exit("Invalid arguments.  You must pass in the path to the setup json")

def writeFilePreamble(outputFile, namespace):
    outputFile.write("#pragma once\n\n")
    outputFile.write("#include \"Image.h\"\n\n")
    outputFile.write("namespace " + namespace + "\n")
    outputFile.write("{\n\n")

def writeFileEpilogue(outputFile):
    outputFile.write("}\n")

def to_camel_case(snake_str):
    components = snake_str.split('_')
    return components[0] + ''.join(x.title() for x in components[1:])

def get_pixel_data(image, x, y):
    width, height = image.size
    if x >= width or y >= height:
        sys.exit("Out of bounds.  Requested x: " + str(x) + ", y: " + str(y) + ", but dimensions are width: " + str(width) + ", height: " + str(height))
    
    channels = 1
    startPixel = (y * width * channels) + (x * channels)
    image_data = image.getdata()

    if startPixel >= len(image_data):
        sys.exit("Out of bounds.  Requested x: " + str(x) + ", y: " + str(y) + ", which is pixels starting from: " + str(startPixel) + ", but image only has: " + str(len(image_data)) + " pixels.")

    return image_data[startPixel]#, image_data[startPixel + 1], image_data[startPixel + 2], image_data[startPixel + 3])

def get_name_from_char(character):

    switcher = {
        " ": "space",
        "!": "exclamation_mark",
        "\"": "double_quote",
        "#": "hash",
        "$": "dollar",
        "%": "percent",
        "&": "ampersand",
        "'": "single_quote",
        "(": "round_bracket_open",
        ")": "round_bracket_close",
        "*": "asterisk",
        "+": "plus",
        ",": "comma",
        "-": "hyphen",
        ".": "full_stop",
        "/": "forward_slash",
        "0": "zero",
        "1": "one",
        "2": "two",
        "3": "three",
        "4": "four",
        "5": "five",
        "6": "six",
        "7": "seven",
        "8": "eight",
        "9": "nine",
        ":": "colon",
        ";": "semi_colon",
        "<": "less_than",
        "=": "equal",
        ">": "greater_than",
        "?": "question_mark",
        "@": "at_symbol",
        "A": "uppercase_a",
        "B": "uppercase_b",
        "C": "uppercase_c",
        "D": "uppercase_d",
        "E": "uppercase_e",
        "F": "uppercase_f",
        "G": "uppercase_g",
        "H": "uppercase_h",
        "I": "uppercase_i",
        "J": "uppercase_j",
        "K": "uppercase_k",
        "L": "uppercase_l",
        "M": "uppercase_m",
        "N": "uppercase_n",
        "O": "uppercase_o",
        "P": "uppercase_p",
        "Q": "uppercase_q",
        "R": "uppercase_r",
        "S": "uppercase_s",
        "T": "uppercase_t",
        "U": "uppercase_u",
        "V": "uppercase_v",
        "W": "uppercase_w",
        "X": "uppercase_x",
        "Y": "uppercase_y",
        "Z": "uppercase_z",
        "[": "squre_bracket_open",
        "\\": "backslash",
        "]": "squre_bracket_close",
        "^": "carat",
        "_": "underscore",
        "`": "backtick",
        "a": "lowercase_a",
        "b": "lowercase_b",
        "c": "lowercase_c",
        "d": "lowercase_d",
        "e": "lowercase_e",
        "f": "lowercase_f",
        "g": "lowercase_g",
        "h": "lowercase_h",
        "i": "lowercase_i",
        "j": "lowercase_j",
        "k": "lowercase_k",
        "l": "lowercase_l",
        "m": "lowercase_m",
        "n": "lowercase_n",
        "o": "lowercase_o",
        "p": "lowercase_p",
        "q": "lowercase_q",
        "r": "lowercase_r",
        "s": "lowercase_s",
        "t": "lowercase_t",
        "u": "lowercase_u",
        "v": "lowercase_v",
        "w": "lowercase_w",
        "x": "lowercase_x",
        "y": "lowercase_y",
        "z": "lowercase_z",
        "{": "curly_bracket_open",
        "|": "pipe",
        "}": "curly_bracket_close",
        "~": "tilde",
        "°": "degrees",
    }

    return switcher.get(character, "NOT_FOUND!")


def writeGlyph(image_file, output_file, name, black_threshold_min, grey_threshold_min, displayWidth, displayXOffset, displayYOffset, glyphX, glyphY, glyphWidth, glyphHeight):
    camel_case_name = to_camel_case(name)
    array_name = camel_case_name + "Array"

    output_file.write("    constexpr uint8_t " + array_name + "[] = {\n")

    nibble = 0

    count = 0

    for y in range(glyphY, glyphY + glyphHeight):
        isFirstNibble = True
        for x in range(glyphX, glyphX + glyphWidth):
            (r, g, b, a) = get_pixel_data(image_file, x, y)
            BLACK = 0
            WHITE = 3
            GREY = 1
            YELLOW = 4

            color = WHITE
            greyThreshold = 2
            if abs(r - g) <= greyThreshold and abs(g - b) <= greyThreshold:
                determiner = a if a < 255 else r
                if determiner >= black_threshold_min:
                    color = BLACK
                elif determiner >= grey_threshold_min:
                    color = GREY
            elif r != 0 or g != 0 or b != 0:
                color = YELLOW

            if count % glyphWidth == 0:
                if count > 0:
                    output_file.write("\n        ")
                    count = 0
                else:
                    output_file.write("        ")

            count = count + 1

            if isFirstNibble:
                nibble = color << 4
            else:
                nibble = nibble | color
                output_file.write("{0:#0{1}x},".format(nibble,4))

            isFirstNibble = not isFirstNibble
    
        if not isFirstNibble:
            output_file.write("{0:#0{1}x},".format(nibble,4))

    if count != 0:
        output_file.write("\n")

    output_file.write("    };\n")

    imageName = camel_case_name + "Image"

    output_file.write("    constexpr Glyph " + imageName + "(" + array_name + ", " + str(glyphWidth) + ", " + str(glyphHeight) + ", " + str(displayWidth) + ", " + str(displayXOffset) + ", " + str(displayYOffset) + ");\n\n")
    return imageName

def writeJSonChars(jsonObject, image_file, output_file, black_threshold_min, grey_threshold_min):

    results = []

    for character in jsonObject['characters']:
        topLeftX = int(character['topLeftX'])
        topLeftY = int(character['topLeftY'])
        bottomRightX = int(character['bottomRightX'])
        bottomRightY = int(character['bottomRightY'])

        if topLeftX >= bottomRightX or topLeftY >= bottomRightY:
            sys.exit("Invalid image dimentions for: " + character['name'])

        width = bottomRightX - topLeftX + 1
        height = bottomRightY - topLeftY + 1

        imageName = writeGlyph(image_file, output_file, character['name'], black_threshold_min, grey_threshold_min, width, 0, 0, topLeftX, topLeftY, width, height)

        results.append((imageName, character['accessor']))

    return results

def writeDivoChars(xml_filename, image_file, output_file, black_threshold_min, grey_threshold_min):
    tree = ET.parse(xml_filename)
    font = tree.getroot()

    results = []

    for char in font:
        accessor = char.attrib['code']
        char_offset = char.attrib['offset'].split(" ")
        rect = char.attrib['rect'].split(" ")

        imageName = writeGlyph(image_file, 
            output_file, 
            get_name_from_char(accessor),
            black_threshold_min,
            grey_threshold_min,
            char.attrib['width'],
            int(char_offset[0]),
            int(char_offset[1]),
            int(rect[0]),
            int(rect[1]),
            int(rect[2]),
            int(rect[3]))

        asciiVal = ord(accessor[0])
        results.append((imageName, asciiVal))

    return results

def writeImageSwitch(output_file, image_tuple_array, font_name):
    output_file.write("    struct " + font_name + " : public GlyphLoader\n")
    output_file.write("    {\n")
    output_file.write("        const Glyph& GetGlyph(uint8_t accessor) const override \n") 
    output_file.write("        {\n")
    output_file.write("           switch (accessor)\n")
    output_file.write("           {\n")

    for (image_name, accessor) in image_tuple_array:
        output_file.write("            case " + str(accessor) + ":\treturn " + image_name + ";\n")

    output_file.write("            default:\n")
    output_file.write("                return emptyGlyph;\n")
    output_file.write("            }\n")
    output_file.write("            abort();\n")
    output_file.write("        }\n")
    output_file.write("    };\n")
    
with open(sys.argv[1]) as json_file:
    data = json.load(json_file)

    print("Opened " + sys.argv[1])

    with Image.open(data['inputFilename'], "r") as image_file:

        print("Opened " + data['inputFilename'])

        if image_file.mode != "RGBA":
            sys.exit("Expected rgba file, got: " + image_file.mode)

        with open(data['outputFilename'] + ".h", "w") as output_file:

            print("Writing to " + data['outputFilename'])
        
            black_threshold_min = data['blackThresholdMin']
            grey_threshold_min = data['greyThresholdMin']

            writeFilePreamble(output_file, data['namespace'])

            images = []

            if 'divoXML' in data is not None:
                images = writeDivoChars(data["divoXML"], image_file, output_file, black_threshold_min, grey_threshold_min)
            else:
                images = writeJSonChars(data, image_file, output_file, black_threshold_min, grey_threshold_min)

            output_file.write("\n\n")
            writeImageSwitch(output_file, images, data['namespace'] + "Loader")

            writeFileEpilogue(output_file)

print("Done")